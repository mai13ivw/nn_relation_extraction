This work is based on

ETH-DS3Lab at SemEval-2018 Task 7: Effectively Combining Recurrent and Convolutional Neural Networks for Relation Classification and Extraction
By Jonathan Rotsztejn, Nora Hollenstein, Ce Zhang
https://arxiv.org/abs/1804.02042

Keras Version: 2.1.5
Python Version: 3.5.3

==============================================================================================================
Set up project
> 'saving paths' = unzip downloaded files here

0. Download relevant data for all domains you want to test

    Downloads that are relevant for all tasks:
    - Install spaCy and python dependencies:
        - Apply spaCy pre-installation instructions at https://spacy.io/usage/#gpu (only needed for GPU-mode)
        - Install python dependencies via `pip install -r requirements[-dev].txt`
        - Download spaCy language model via `python -m spacy download en`

    - Download the Glove Dataset from https://nlp.stanford.edu/projects/glove/ (glove.6B.zip)
        - saving path: 'resources/'

    Downloads for SemEval-2018 Task 7
    - Download all training and test files for both tasks from https://lipn.univ-paris13.fr/~gabor/semeval2018task7/
        - saving path: 'resources/semEval/raw/'
    - Download the ACL ARC cleaned dataset from https://web.eecs.umich.edu/~lahiri/acl_arc.html 
        - saving path of all .txt files: 'resources/semEval/embeddings/ACL_raw/'

    Downloads for BioNLP-2011
    - Download all files from https://github.com/davidsbatista/Annotated-Semantic-Relationships-Datasets/blob/master/datasets/BioNLP.tar.gz
        - saving path: 'resources/BioNLP/raw/'

    Downloads for ScienceIE
    - Download the Training data from https://scienceie.github.io/resources.html
        - saving path: 'resources/scienceIE/raw/' 

    Downloads for SemEval-2010 Task 8
    - Download all files from https://github.com/davidsbatista/Annotated-Semantic-Relationships-Datasets/blob/master/datasets/SemEval2010_task8_all_data.tar.gz
        - saving path: 'resources/semEval2010/raw/'

1. Preprocess training and test corpora

    SemEval-2018 Task 7-1, go to 'preprocessing/semEval':
        python preprocess.py flatten
        python preprocess.py complete 
        python preprocess.py complete --corpus=validation

    BioNLP-2011 go to 'preprocessing/BioNLP':
        python preprocess.py complete

    scienceIE go to 'preprocessing/scienceIE':
        python preprocess.py complete

    SemEval-2010 Task 8, go to 'preprocessing/semEval2010':
        python preprocess.py complete
        python preprocess.py complete --corpus=validation

    If you want to get data for SemEval-2018 Task 7-2, the results for task 1 are needed!
    So you can only use the following commands after getting the predictions for Subtask 1
        python preprocess.py complete --task=2
        python preprocess.py complete --task=2 --corpus=validation
        

    If you want to check all steps seperately (also usefull for debugging), you can run each mode alone.
    Keep in mind that POS tagging will take a while (up to an hour or longer)
    The finished corpora are saved here: resources/{domain}/preprocessed/corpora/

2. Set parameters

    Parameter setting are found in model_params.json
    Before doing any training you can change everything as you want to.
    Explanations:
        "weights" in {0, 1} -> Do you want to use weighted labels for training?
        "upsampling" in {0, 1} -> Do upsampling for all classes with the NONE class as measure?
        "upsample-rate" in {0..1} -> Only relevant when upsampling is used. Set ratio with respect to the amount of NONE-label examples.
        "embedding" in {0, 50, 100, 200, 300, x} -> 0 means you have your own embedding, the over values indicate a corresponding glove-embedding. Any other number means you use easyGlove (sample from glove).
        "max_sent_len" in N -> length of longest sentence in training data?
        "min_sent_len" in N -> length of shortest sentence in training data?
            - max sentence lengths are set longer, in case test data is longer than training data

3. Create word embeddings

    If you want to create your own word embedding, do the following:
    Go to 'core/embeddings/' and run the following commands:

        SemEval-2018 Task 7:
            python word2vec.py clean --domain=semEval
            python word2vec.py train --domain=semEval

        BioNLP-2011:
            python word2vec.py clean --domain=BioNLP
            python word2vec.py train --domain=BioNLP

        SemEval-2011 Task 8:
            python word2vec.py clean --domain=semEval2010
            python word2vec.py train --domain=semEval2010

        ScienceIE:
            python word2vec.py clean --domain=scienceIE
            python word2vec.py train --domain=scienceIE

    All Embeddings are saved here: 'resources/{domain}/embeddings/word_emb.txt'

    To test the big embedding for BioNLP, you can download all mentioned data from
    http://bio.nlplab.org and https://www.ncbi.nlm.nih.gov/CBBresearch/Dogan/DISEASE/
    Add text files to resources/BioNLP/embedding/raw and change min_words for word2vec from 1 to 10

4. It is time to train some Systems!
    IMPORTANT: Cuda won't work for CNNs.
        As a workaround I set 'export CUDA_VISIBLE_DEVICES=1' for LSTMs and 'export CUDA_VISIBLE_DEVICES=2' for CNNs
    The main file for training is model_train.py
    After creating all training, validation and embedding files, you can train all networks:

    SemEval-2018 Task 7-1:
        - The number of epochs must be changed in model_params.json
        - (20 - 50) epochs seem to work well, too, instead of 200
        python model_train.py --task=1 --model=LSTM
        python model_train.py --task=1 --model=CNN

    SemEval-2010 Task 8:
       python model_train.py --model=LSTM --domain=semEval2010
       python model_train.py --model=CNN --domain=semEval2010

    BioNLP-2011:
       python model_train.py --model=LSTM --domain=BioNLP
       python model_train.py --model=CNN --domain=BioNLP

    scienceIE:
       python model_train.py --model=LSTM --domain=scienceIE
       python model_train.py --model=CNN --domain=scienceIE

    Since K fold cross validation is used, a lot of models must be trained and this can take up some time!

    SemEval-2018 Task 7-2 still needs predictions of SubTask 1. After getting all data, you can also train this model:
        python model_train.py --task=2 --model=LSTM
        python model_train.py --task=2 --model=CNN

5. Testing the trained systems:
    For testing the system is necessary to have a trained lstm and/or cnn.
        If you have models for task 1, you need to create NR files. These files contain, whether a relation is NORMAL or REVERSED.
        So if the Systems labels a relation as 'compare' but its type is reversed, it chooses the second best label.
        Go to 'preprocessing/semEval/helper/' and run
            python createNRlist.py

    If you want to test your ensemble of models, run the following commands:

        SemEval-2018 Task 7-1:
            python model_test_many.py --amount=5 --task=1 --domain=semEval
            (This command also creates a prediction file! So now you can create the training data for Subtask 2)

        SemEval-2018 Task 7-2:
            python model_test_many.py --amount=5 --task=2 --domain=semEval

        SemEval-2010 Task 8:
            python model_test_many.py --amount=5 --domain=semEval2010

        BioNLP:
            python model_test_many.py --amount=5 --domain=BioNLP

        scienceIE:
            python model_test_many.py --amount=5 --domain=scienceIE
        
    For testing single models or model-pairs, use 'model_train.py'

        python model_test.py {--task=x} --cnnModel={name1} --lstmModel={name2} --domain={domain}

        This also works with only a cnnModel or lstmModel. However, combined results are better!
        Names are the timestamps, so for example you only have to type --cnnModel=2705_104429

==============================================================================================================
Preprocess single sentences for subtask 2

    For this kind of Preprocessing use 'preprocessing/PreprocessSentence.py'.

==============================================================================================================
With POS_viewer you can check the POS-Tag-Distributions for all relations of each task
    
    python POS_viewer --domain=semEval

==============================================================================================================
Usecase: Process a single input sentence and give a relation as result (TODO not finished)

    Use the module usecase.py to test a single sentence:
    python usecase.py --cnnModel=... --lstmModel=...

    When the models are loaded you can type your sentences.
    A Sentence must have the following format:
        <e> entitiy1 </e> some words between <e> entitiy2 </e> and more words and relations ...
    
    The system will check for relations between all pairs of entities and print the results

==============================================================================================================
==============================================================================================================
Extra: If an error appears pointing that your files aren't UTF-8:
    go to the folder 'toUTF8' and run one of the scripts with your data path

