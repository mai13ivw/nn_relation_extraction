from keras.models import model_from_json
import os, sys
from os.path import isfile, join

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
RESOURCES_FOLDER = os.path.join(module_location, '../resources/')

def loadModel(name, domain, task=1, type='CNN'):
    """Load a certain, trained NN

    Args:
        param1 (str): name of the NN (without Network type)
        param2 (str): domain used
        param3 (int): task is only relevant for SemEval-2018
        param4 (str): model type, must be CNN or LSTM
    
    Returns:
        Keras.model: loaded model
    
    """
    type = type.upper()
    if name == 'undef':
        print("{}-model is not defined".format(type))
        return None

    if domain == 'semEval':
        json_path = RESOURCES_FOLDER + domain + '/models/task_' + str(task) + '/' + type + '_' + name + '.json'
        weights   = RESOURCES_FOLDER + domain + '/models/task_' + str(task) + '/' + type + '_' + name + '.h5'
    else:
        json_path = RESOURCES_FOLDER + domain + '/models/' + type + '_' + name + '.json'
        weights   = RESOURCES_FOLDER + domain + '/models/' + type + '_' + name + '.h5'

    json_file = open(json_path, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)

    # Load Model weights HDF5 for LSTM #
    loaded_model.load_weights(weights)
    print('> {}-model successfully loaded!'.format(type))
    return loaded_model

def getModelNamesFromFolder(myPath):
    """Get names from models in a folder

    Args:
        param1 (str): path of folder

    Returns:
        list: model names

    """
    onlyfiles = [f for f in os.listdir(myPath) if isfile(join(myPath, f))]

    fileNames = set([])
    for f in onlyfiles:
        if 'log' not in f:
            split = f.split('.')
            name = '.'.join(split[:-1])
            fileNames.add(name)

    cleanedData = []
    for name in fileNames:
        split = name.split('_')
        type = split[0]
        newName = '_'.join(split[1:])
        cleanedData.append([type, newName])

    return cleanedData

def loadModelsInFolder(path, type, domain, amount, task):
    """Load models of a specific type

    Use the created list by getModelNamesFromFolder to load models

    Args:
        param1 (str): path where models are saved
        param2 (str): type of model, CNN or LSTM
        param3 (str): domain used
        param4 (int): amount of models used. Number must always match the number of models in a folder / 2
        param5 (int): task is only relevant for semEval-2018

    """
    fileNames = getModelNamesFromFolder(path)
    assert amount == len(fileNames) / 2
    print("Models for {}".format(type))

    loadedModels = {}
    for file in fileNames:
        if file[0] == type:
            print(file)
            m = loadModel(file[1], domain, task, type=type)
            loadedModels[file[1].split('_')[-1]] = m
            if len(loadedModels) == amount:
                break
    return loadedModels

# Test Loading some models
if __name__ == '__main__':
    domain = 'semEval'
    model  = 'CNN'
    task   = 1
    amount = 2

    myPath = RESOURCES_FOLDER + domain + '/models/task_' + str(task)
    models = loadModelsInFolder(myPath, model, domain, amount, task)

    print("> Menge geladener Modelle: {}".format(len(models)))