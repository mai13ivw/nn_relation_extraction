# -*- coding: utf-8 -*-

import gensim, sys, re, os, json, numpy as np
from tqdm import tqdm

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
RESOURCES_FOLDER = os.path.join(module_location, '../resources/')

with open(os.path.join(module_location, '../../model_params.json')) as f:
    model_params = json.load(f)

all_zeroes       = "ALL_ZERO"
unknown          = "_UNKNOWN"

def load(path):
    """Loads pre-trained embeddings

    the data format must be like Glove: 
    [word value_1 value_2 ... value_n] per line.
    Also add an unknown_word for out-of-vocabulary cases.
    This word is the average of the last 100 embeddings.
    Also add an embedding that only contains zeros.

    Args:
        param1 (str): path to embedding
    
    Returns:
        numpy.array: values for each embedding as floats
        dict: words with corresponding indices to the numpy.array

    .. Source:
        https://github.com/UKPLab/emnlp2017-relation-extraction

    """
    word2idx = {}
    embeddings = []

    with open(path, 'r', encoding='utf8') as fIn:
        idx = 1               
        for line in tqdm(fIn):
            split = line.strip().split(' ')                
            embeddings.append(np.array([float(num) for num in split[1:]]))
            word2idx[split[0]] = idx
            idx += 1
    
    # add ALL_ZERO
    word2idx[all_zeroes] = 0
    embedding_size       = embeddings[0].shape[0]
    embeddings           = np.asarray([[0.0]*embedding_size] + embeddings, dtype='float32')
    
    # add UNKNOWN
    if idx > 100:
        rare_w_ids = list(range(idx-101,idx-1))
    else:
        rare_w_ids = list(range(0, idx-1))

    unknown_emb = np.average(embeddings[rare_w_ids,:], axis=0)
    embeddings = np.append(embeddings, [unknown_emb], axis=0)
    word2idx[unknown] = idx
    idx += 1
    
    return embeddings, word2idx

def get_idx(word, word2idx):
    """Get the word index for a given word

    If the word isn't found, it checks for lowercase.
    If lowercase also doesn't word, a trimmed version is used.
    If trim doesn't work, numbers will be cleared, too.
    If the word still isn't found, return index to unknown words.

    Args:
        param1 (str): word to look for
        param2 (dict): dictionary that contains all words and indices

    Returns:
        int: indice for the word

    .. Source:
        https://github.com/UKPLab/emnlp2017-relation-extraction
    
    """
    unknown_idx = word2idx[unknown]
    word = word.strip()
    if word in word2idx:
        return word2idx[word]
    elif word.lower() in word2idx:
        return word2idx[word.lower()]
    trimmed = re.sub("(^\W|\W$)", "", word)
    if trimmed in word2idx:
        return word2idx[trimmed]
    elif trimmed.lower() in word2idx:
        return word2idx[trimmed.lower()]
    no_digits = re.sub("([0-9][0-9.,]*)", '0', word)
    if no_digits in word2idx:
        return word2idx[no_digits]
    return unknown_idx

def get_word_sequence(word_ids, word2idx):
    """Try to convert a sequence of indices to words

    This is used for debugging and won't work always,
    because word -> id is not 100% reversable.

    Args:
        param1 (list): list with all indices
        param2 (dict): dictionary that contains all words and indices

    Returns:
        list: word sequence

    """
    words = []
    for id in word_ids:
        for key in word2idx:
            if id == word2idx[key]:
                words.append(key)
                break
    return words

def get_idx_sequence_with_dict(dict, sequence):
    vector = []
    for word in sequence:
        if word in dict:
            vector.append(dict[word])
        else:
            vector.append(dict['NONE'])
    return vector

def get_idx_sequence(word_sequence, word2idx):
    """Get indices for a word sequence

    Args:
        param1 (list): sequence of words
        param2: (dict): dictionary that contains all words and indices

    Returns:
        list: sequence of indices

    .. Source:
        https://github.com/UKPLab/emnlp2017-relation-extraction
    """
    vector = []
    for word in word_sequence:
        word_idx = get_idx(word, word2idx)
        vector.append(word_idx)
    return vector

def to_one_hot(labelIds, num_classes=None):
    """Convert vector with integers to a binary matrix.

    Args:
        param1 (list): relevant indices
        param2 (integer, optional): total number of different classes used

    Returns:
        np.array: binary matrix representation

    .. Source:
        https://github.com/UKPLab/emnlp2017-relation-extraction

    >>> print(to_onte_hot([1,3,2,2,1]))
    [[0,1,0,0], [0,0,0,1], [0,0,1,0], [0,0,1,0], [0,1,0,0]]

    """
    y = np.array(labelIds, dtype='int')

    if not num_classes:
        num_classes = np.max(y) + 1

    # create matrix out of number of elements
    n = y.shape[0]
    categorical = np.zeros((n, num_classes))
    categorical[np.arange(n), y] = 1

    return categorical