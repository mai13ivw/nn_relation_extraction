from tqdm import tqdm

def getMacroF1(labels, idx2label, predictions_set, keys_set, domain):
    """Get Macro F1 score for given predictions, keys and labels
    
    You can also use a list of matrices that save preditions,
    This is useful for task 2 of semEval-2018

    Args:
        param1 (list): all labels
        param2 (dict): mapping from indices to labels
        param3 (list): all predictes made by a model
        param4 (list): golden standard used to compare with the model
        param5 (str): domain used

    Returns:
        list: label_scores for each label. Every label gets 6 values
        >>> L = label_scores[0]
        L[0] = true positives
        L[1] = false positives
        L[2] = false negatives
        L[3] = precision
        L[4] = recall
        L[5] = F1-Score

        >>> macro_F1 = label_scores['ALL']

    """
    empty_arr = [0,0,0,0,0,0]  #tp fp fn prec rec f1
    labels_scores    = {}
    for k in list(labels.keys()):
        labels_scores[k] = empty_arr[:]

    sum_prec = 0
    sum_rec  = 0

    # Go through all sentence matrices #
    for predictions, keys in tqdm(zip(predictions_set, keys_set)):
        keys = keys[-1]

        # Get precision and recall for all labels #
        for label in labels_scores:
            for pred, y in zip(predictions, keys):
                if pred == label:
                    if idx2label[y] == label:
                        labels_scores[label][0] += 1
                    else:
                        labels_scores[label][1] += 1
                else:
                    if idx2label[y] == label:
                        labels_scores[label][2] += 1

    # The NONE label is not relevant for the semEval-2010 score
    if domain == 'semEval2010':
        del labels_scores['NONE']
        
    for label in labels_scores:

        tp = labels_scores[label][0]
        fp = labels_scores[label][1]
        fn = labels_scores[label][2]

        if (tp + fp) != 0 and (tp + fn) != 0 and tp != 0:
            labels_scores[label][3] = tp / (tp + fp)
            labels_scores[label][4] = tp / (tp + fn)

            prec = labels_scores[label][3]
            rec  = labels_scores[label][4]
            sum_prec += prec
            sum_rec  += rec

            f1 = (2 * prec * rec) / (prec + rec)
        else:
            f1 = 0
        labels_scores[label][5] = f1

    avg_prec = sum_prec / len(labels_scores)
    avg_rec  = sum_rec  / len(labels_scores)
    macro_f1 = (2 * avg_prec * avg_rec) / (avg_prec + avg_rec)
    labels_scores['ALL'] = [-1, -1, -1, avg_prec, avg_rec, macro_f1]
    return labels_scores

def printScores(scores):
    """Show formatted scores reached by all labels

    List precision, recall and f1 for all labels,
    the last value is a Macro-F1-Score

    Args:
        param1 (list): scores calculated by getMacroF1
        
    """
    for score in scores:
        if score != 'ALL':
            values = scores[score]
            out = score + ': Prec = ' + str(round(values[3], 4)) + ', Rec = ' + str(round(values[4], 4)) + ', F1 = ' + str(round(values[5], 4))
            print(out)

    score = 'ALL'
    values = scores[score]
    out = score + ': Prec = ' + str(round(values[3], 4)) + ', Rec = ' + str(round(values[4], 4)) + ', F1 = ' + str(round(values[5], 4))
    print(out)