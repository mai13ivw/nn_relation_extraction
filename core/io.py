import random, os, ast

META_SPLIT       = ';;'
module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
RESOURCES_FOLDER = os.path.join(module_location, '../resources/')

def load_data(path, keepBreaks):
    """Load lines of a file and save them to a list
    
    Args:
        param1 (str): Path to file
        param2 (str): check if line breaks should be saved, too

    Returns:
        list: all relevant lines
    
    """
    samples = []
    with open(path, 'r', encoding="utf-8") as fIn:
            for line in fIn:
                if line.strip() == '':
                    if not keepBreaks:
                        continue
                samples.append(line.replace('\n', ''))
    return samples

def load_data_2(path):
    """Load preprocessed sentences 
    
    Each sentence contains multiple lines, save those lines to a sublist.
    This function is used for postprocessing semEval-2018 Task 2

    Args:
        param1 (str): Path to file

    Returns:
        list: All seperate sentences, that also contain each line

    """
    samples     = []
    sampleBatch = []
    with open(path, 'r') as fIn:
        for line in fIn:
            if line.strip() == '':
                samples.append(sampleBatch[:])
                sampleBatch = []
            else:
                sampleBatch.append(line.replace('\n', ''))
        samples.append(sampleBatch[:])
    return samples

def load_data_downsampled(path, neg_label, labels, ratio = -1):
    """Same idea as upsampling, however examples from the biggest class are removed

    Args:
        param1 (str): Path to file
        param2 (str): neg_label name
        param3 (list): all labels used
        param4 (int, optional): Set a ratio (negative to positive) with ratio > 0. If non is set, the ratio is chosen randomly [0.5, 2.5]
    
    Returns:
        list: upsampled data
    
    """

    if ratio == -1:
        ratio = random.uniform(0.5, 2.5)
    else:
        assert ratio > 0

    neg_samples     = []
    samples         = {}
    num_pos_samples = 0
    num_neg_samples = 0

    # Create empty lists for all possible labels #
    for label in labels:
        samples[label] = []

    # Read sentences #
    with open(path, 'r', encoding="utf-8") as fIn:
        for line in fIn:
            if line.strip():
                label = line.split(';;')[:-1]
                label = '_'.join(label)
                if label != neg_label:
                    num_pos_samples += 1
                samples[label].append(line.replace('\n', ''))

    neg_samples = samples[neg_label]
    num_neg_samples = len(neg_samples)

    # Don't use more neg samples than there actually are
    assert (num_pos_samples * ratio) <= num_neg_samples

    # number of negative samples that should be used #
    num_neg_samples = int(num_pos_samples * ratio)

    # generate random (unique) indices to choose from the negative samples
    new_neg_samples = []
    idx_list = random.sample(range(len(samples[neg_label])), num_neg_samples)
    for idx in idx_list:
        new_neg_samples.append(neg_samples[idx])
    samples[neg_label] = new_neg_samples

    # create new array #
    all_samples = []
    for sample in samples:
        all_samples.extend(samples[sample])

    random.shuffle(all_samples)
    return all_samples

def saveResults(results, dest_path):
    """Save all results to a new file

    Args:
        param1 (list): samples
        param2 (str): destination path

    """
    with open(dest_path, 'w') as fOut:
        for result in results:
            fOut.write(result + '\n')