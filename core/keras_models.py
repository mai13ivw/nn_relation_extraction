
import os, json, ast, numpy as np
from core import labels
from core.embeddings  import embeddings
from tqdm  import tqdm
from keras import models, optimizers, regularizers, backend as K
from keras.layers import LSTM, Bidirectional, Embedding, Dropout, Dense, Input, Conv1D, MaxPooling1D, Flatten, Concatenate
from keras.regularizers import l2
module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)

with open(os.path.join(module_location, '../model_params.json')) as f:
    model_params = json.load(f)

RESOURCES_FOLDER = os.path.join(module_location, '../resources/')
META_SPLIT       = ';;'

# create a dict for POS-Tags with indices
pos_tags_dict = {}
with open(RESOURCES_FOLDER + 'posTags.txt') as f:
    tags = f.read().split(' ')
    for i, t in enumerate(tags):
        pos_tags_dict[t] = i + 1
    pos_tags_dict['NONE'] = len(pos_tags_dict) + 1

# indices for relative positions
rel_positions_dict = {}

def initRelPositionDict(domain):
    """ initialize position indices

    Args:
        param1 (str): domain name used
        >>> initRelPositionDict('BioNLP')

    """
    for i, t in enumerate(range(-model_params[domain]['max_sent_len'], model_params[domain]['max_sent_len'])):
        rel_positions_dict[str(t)] = i + 1
    rel_positions_dict['NONE'] = len(rel_positions_dict) + 1

# customized loss function with weights 
def weighted_categorical_crossentropy(weights):
    """A weighted version of keras.objectives.categorical_crossentropy
    
    Args:
        param1 (numpy.array): all weights, shape = (C,) where C is the number of classes
    
    Returns:
        float: loss depending on weight

    Usage:
        weights = np.array([0.5,2,10]) # Class one at 0.5, class 2 twice the normal weights, class 3 10x.
        loss = weighted_categorical_crossentropy(weights)
        model.compile(loss=loss,optimizer='adam')

    .. Source:
        https://gist.github.com/wassname/ce364fddfc8a025bfab4348cf5de852d

    """
    weights = K.variable(weights)
        
    def loss(y_true, y_pred):
        # scale predictions so that the class probas of each sample sum to 1
        y_pred /= K.sum(y_pred, axis=-1, keepdims=True)
        # clip to prevent NaN's and Inf's
        y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
        # calc
        loss = y_true * K.log(y_pred) * weights
        loss = -K.sum(loss, -1)
        return loss
    return loss

def to_indices(samples, word2idx, label2idx, domain, predict=False):
    """Create numpy arrays and matrices for training a network

    Args:
        param1 (list): all samples used in a data set
        param2 (dict): dictionary that maps words to integers
        param3 (dict): dictionary that maps labels to indices
        param4 (str): domain used
        param5 (boolean, optional): if True, the input doesn't contain labels. This is implemented for a usecase

    Returns:
        np.array: matrix with all indices in seperate matrices

    """
    sentence_matrix  = np.zeros((len(samples), model_params[domain]['max_sent_len']), dtype="int32")
    POS_matrix       = np.zeros((len(samples), model_params[domain]['max_sent_len']), dtype="int8")
    lposition_matrix = np.zeros((len(samples), model_params[domain]['max_sent_len']), dtype="int16")
    rposition_matrix = np.zeros((len(samples), model_params[domain]['max_sent_len']), dtype="int16")
    y_matrix = np.zeros((len(samples), ), dtype="int16")
    
    # Go through all sentences (samples) and create the matrices #
    for i, sample in enumerate(tqdm(samples, ascii=True)):
        if len(sample) == 0:
            continue
            
        # Get the correct label id for the specific sample and task #
        splits = sample.split(META_SPLIT)
        if(len(splits) > 2):
            label = '_'.join(splits[:-1])
        else:
            label = (sample.split(META_SPLIT))[0]

        label_id = None
        if not predict:
            if not label:
                continue
            label_id = label2idx[label]

        sentence    = (sample.split(META_SPLIT))[-1]
        tokens      = [t.split(',')[0] for t in sentence.split('|')]
        pos_tags    = [t.split(',')[1] for t in sentence.split('|')]
        l_positions = [t.split(',')[2] for t in sentence.split('|')]
        r_positions = [t.split(',')[3] for t in sentence.split('|')]
        length      = len(tokens)

        token_sent_ids = embeddings.get_idx_sequence(tokens, word2idx)
        POS_sent_ids   = embeddings.get_idx_sequence_with_dict(pos_tags_dict, pos_tags)
        l_position_ids = embeddings.get_idx_sequence_with_dict(rel_positions_dict, l_positions)
        r_position_ids = embeddings.get_idx_sequence_with_dict(rel_positions_dict, r_positions)

        # Fill input matrices X #
        sentence_matrix[i, :length]  = token_sent_ids
        POS_matrix[i, :length]       = POS_sent_ids
        lposition_matrix[i, :length] = l_position_ids
        rposition_matrix[i, :length] = r_position_ids

        # Fill output vector Y #
        if not predict:
            y_matrix[i] = label_id
    return [sentence_matrix, POS_matrix, lposition_matrix, rposition_matrix, y_matrix]

def LSTM_model(word_emb_matrix, weights, label2idx, task, domain):
    """Create a keras LSTM-model

    Args:
        param1 (numpy.array): matrix with all words and their vector embeddings
        param2 (list): weights for all labels in correct order
        param3 (dict): mapping from labels to indices
        param4 (int): only relevant for semEval-2018
        param5 (str): domain used
    
    Returns:
        Model: new BLSTM model

    """
    # ---------------------- Parameters section -------------------
    #
    max_sent_len = model_params[domain]['max_sent_len']
    dropout      = model_params['dropout']
    lstmUnits    = model_params['unitsLSTM']
    n_out        = len(label2idx)
    #
    # ---------------------- Parameters end -----------------------

    sentence_input  = Input(shape=(max_sent_len,), dtype='int32', name='sentence_input')
    word_embeddings = Embedding(output_dim   = word_emb_matrix.shape[1], 
                                input_dim    = word_emb_matrix.shape[0],
                                input_length = max_sent_len, 
                                weights      = [word_emb_matrix],
                                mask_zero    = True, 
                                trainable    = False)(sentence_input)

    POS_input      = Input(shape=(max_sent_len,), dtype='int32', name='POS_input')
    POS_embeddings = Embedding(input_dim    = len(pos_tags_dict)+1, 
                               output_dim   = 30,
                               input_length = max_sent_len)(POS_input)
    
    lposition_input      = Input(shape=(max_sent_len,), dtype='int32', name='lposition_input')
    lposition_embeddings = Embedding(input_dim    = len(rel_positions_dict)+1, 
                                     output_dim   = 20,
                                     input_length = max_sent_len)(lposition_input)

    rposition_input      = Input(shape=(max_sent_len,), dtype='int32', name='rposition_input')
    rposition_embeddings = Embedding(input_dim    = len(rel_positions_dict)+1, 
                                     output_dim   = 20,
                                     input_length = max_sent_len)(rposition_input)

    # Concatenate embeddings #
    con_input = Concatenate()([word_embeddings, POS_embeddings, lposition_embeddings, rposition_embeddings])

    # Create network layers #
    lstm_layer      = LSTM(lstmUnits, 
                            return_sequences=False, 
                            kernel_regularizer=regularizers.l2(0.01))
    blstm_layer     = Bidirectional(lstm_layer)
    sentence_vector = blstm_layer(con_input)

    #Add Dropout, get last layer #
    sentence_vector = Dropout(dropout)(sentence_vector)
    main_output     = Dense(n_out, activation="softmax", name='main_output', kernel_regularizer=regularizers.l2())(sentence_vector)

    # Initialize a new model with the given layers #
    model = models.Model(inputs=[sentence_input, POS_input, lposition_input, rposition_input], outputs=[main_output])

    # Compile model #
    if weights:
        model.compile(optimizer='adam', loss=weighted_categorical_crossentropy(weights), metrics=['accuracy'])
    else:
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return model

def CNN_model(word_emb_matrix, weights, label2idx, task, domain):
    """Create a keras CNN-model

    Args:
        param1 (numpy.array): matrix with all words and their vector embeddings
        param2 (list): weights for all labels in correct order
        param3 (dict): mapping from labels to indices
        param4 (int): only relevant for semEval-2018
        param5 (str): domain used
    
    Returns:
        Model: new BLSTM model

    """
    # ---------------------- Parameters section -------------------
    #
    max_sent_len = model_params[domain]['max_sent_len']
    dropout      = model_params['dropout']
    num_filters  = model_params['numCNNfilter']
    filter_sizes = model_params['CNNfilterSizes']
    n_out        = len(label2idx)
    #
    # ---------------------- Parameters end -----------------------

    sentence_input  = Input(shape=(max_sent_len,), dtype='int32', name='sentence_input')
    word_embeddings = Embedding(output_dim   = word_emb_matrix.shape[1], 
                                input_dim    = word_emb_matrix.shape[0],
                                input_length = max_sent_len, 
                                weights      = [word_emb_matrix],
                                trainable    = False)(sentence_input)

    POS_input      = Input(shape=(max_sent_len,), dtype='int32', name='POS_input')
    POS_embeddings = Embedding(input_dim    = len(pos_tags_dict)+1, 
                               output_dim   = 30,
                               input_length = max_sent_len)(POS_input)
    
    lposition_input      = Input(shape=(max_sent_len,), dtype='int32', name='lposition_input')
    lposition_embeddings = Embedding(input_dim    = len(rel_positions_dict)+1, 
                                     output_dim   = 20,
                                     input_length = max_sent_len)(lposition_input)

    rposition_input      = Input(shape=(max_sent_len,), dtype='int32', name='rposition_input')
    rposition_embeddings = Embedding(input_dim    = len(rel_positions_dict)+1, 
                                     output_dim   = 20,
                                     input_length = max_sent_len)(rposition_input)

    # Concatenate embeddings #
    con_input = Concatenate()([word_embeddings, POS_embeddings, lposition_embeddings, rposition_embeddings])
    
    # Add all filter lengths to the CNN #
    conv_blocks = []
    for sz in filter_sizes:
        conv = Conv1D(num_filters, 
                      sz, 
                      padding='valid',
                      activation='relu', 
                      strides=1)(con_input)
        conv = MaxPooling1D(pool_size=2)(conv)
        conv = Flatten()(conv)
        conv_blocks.append(conv)

    # Concatenate filter outputs and calculate a dropout #
    z = Concatenate()(conv_blocks) if len(conv_blocks) > 1 else conv_blocks[0]
    z = Dropout(dropout)(z)

    main_output = Dense(n_out, 
                        kernel_regularizer=regularizers.l2(),
                        activation="softmax", 
                        name='main_output')(z)

    # Initialize a new model with the given layers #
    model = models.Model(inputs=[sentence_input, POS_input, lposition_input, rposition_input], outputs=[main_output])

    # Compile model #
    if weights:
        model.compile(optimizer='adam', loss=weighted_categorical_crossentropy(weights), metrics=['accuracy'])
    else:
        model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
    return model