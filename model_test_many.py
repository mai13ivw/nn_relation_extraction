import sys, json
from core import keras_models, io, score, labels as myLabels
from core.loadModel import loadModelsInFolder
from core.embeddings import embeddings
from keras.models import model_from_json
from predicting import task1, task2
from postprocessing import task2 as post2
from tqdm import tqdm

RESOURCES_FOLDER  = 'resources/'
META_SPLIT        = ';;'
with open('model_params.json') as f:
    model_params = json.load(f)
        
if __name__ == "__main__":
    import argparse

    ####################################################################################################
    # Load Args Parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('--domain', default='semEval', choices=['semEval', 'BioNLP', 'semEval2010', 'scienceIE'])
    parser.add_argument('--task', default='1', choices=['1', '2'])
    parser.add_argument('--amount', default=1)
    parser.add_argument('--mode', default='all', choices=['all', 'cnn', 'lstm'])

    args    = parser.parse_args()
    domain  = args.domain
    task    = args.task
    amount  = int(args.amount)
    mode    = args.mode

    if(model_params[domain]['embedding'] == 50):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.50d.txt'
    elif(model_params[domain]['embedding'] == 100):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.100d.txt'
    elif(model_params[domain]['embedding'] == 200):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.200d.txt'
    elif(model_params[domain]['embedding'] == 300):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.300d.txt'
    elif(model_params[domain]['embedding'] == 0):
        WORD_EMB_PATH = RESOURCES_FOLDER + domain + '/embeddings/word_emb.txt'
    else:
        WORD_EMB_PATH = RESOURCES_FOLDER + 'easyGlove.txt'

    keras_models.initRelPositionDict(domain)
    
    ####################################################################################################
    # Data paths and labels
    if domain == 'semEval':
        modelsPath = RESOURCES_FOLDER + 'semEval/models/task_' + task
        val_path  = RESOURCES_FOLDER + domain + '/preprocessed/corpora/' + task + '.validation.txt'
        label2idx = getattr(myLabels, "label2idx_semEval_" + task)
        idx2label  = {v: k for k, v in label2idx.items()}
    else:
        modelsPath = RESOURCES_FOLDER + domain + '/models/'
        val_path  = RESOURCES_FOLDER + domain + '/preprocessed/corpora/validation.txt'
        label2idx = getattr(myLabels, "label2idx_" + domain)
        idx2label  = {v: k for k, v in label2idx.items()}
    
    ####################################################################################################
    # Load Word Embeddings and models
    word_emb_matrix, word2idx = embeddings.load(WORD_EMB_PATH)

    loaded_models_lstm = [None for i in range(0, amount)]
    loaded_models_cnn  = [None for i in range(0, amount)]

    if mode == 'cnn' or mode == 'all':
        loaded_models_cnn  = loadModelsInFolder(modelsPath, 'CNN', domain, amount, task)
    if mode == 'lstm' or mode == 'all':
        loaded_models_lstm = loadModelsInFolder(modelsPath, 'LSTM', domain, amount, task)

    combined_models = []
    for key in loaded_models_cnn:
        print('Model: {}'.format(key))
        combined_models.append((key, loaded_models_cnn[key], loaded_models_lstm[key]))
    
    maxSent = model_params[domain]['max_sent_len']
    minSent = model_params[domain]['min_sent_len']

    ####################################################################################################
    # Predictions for task 1 and semEval
    if domain == 'semEval' and task == '1':
        types = []
        val_NR_path = RESOURCES_FOLDER + domain + '/preprocessed/corpora/1.validation.NR.txt'
        with open(val_NR_path, 'r') as fIn:
            for line in fIn:
                types.append(line)

        predict_data   = io.load_data(val_path, keepBreaks=False)
        predict_matrix = keras_models.to_indices(predict_data, word2idx, label2idx, domain)

        # Get predictions #
        predictions = [task1.predictMany(types, predict_matrix[:-1], combined_models, maxSent, minSent, idx2label)]
        scores = score.getMacroF1(label2idx, idx2label, predictions, [predict_matrix], domain)
        score.printScores(scores)

        # save prediction data for task 2 (additional training data) #
        task1.savePredictions(predictions) 
        sys.exit(0)

    ####################################################################################################
    # Any other predictions for task 2 and domains
    predict_data_set = io.load_data_2(val_path)

    # Indices are saved for every sentence seperately #
    predict_matrices = []
    for predict_data in tqdm(predict_data_set):
        p_data = keras_models.to_indices(predict_data, word2idx, label2idx, domain)
        predict_matrices.append(p_data)

    # Get predictions for every sentence #
    prediction_set = []
    probability_set = []
    for predict_matrix in tqdm(predict_matrices):
        predictions, probabilities, = task2.predictMany(predict_matrix[:-1], combined_models, maxSent, minSent, idx2label)
        prediction_set.append(predictions)
        probability_set.append(probabilities) 

    if domain == 'semEval':
        print('Delete multiple relations...')
        prediction_set = post2.postProcessing(predict_data_set, prediction_set, probability_set)

    # Output as correct and predicted labels for manual testing
    #for s, p in zip(predict_data_set, prediction_set):
    #    print("Correct: {}; Predicted: {}".format(s.split(';;')[0], p))
        
    scores = score.getMacroF1(label2idx, idx2label, prediction_set, predict_matrices, domain)
    score.printScores(scores)