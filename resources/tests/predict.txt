PART_WHOLE;;<e>,EEE,0,-8|arguments,NNS,0,-7|<e>,EEE,0,-6|are,VBP,1,-5|usually,RB,2,-4|limited,VBN,3,-3|in,IN,4,-2|a,DT,5,-1|<e>,EEE,6,0|syntax,NN,7,0|subtree,NN,8,0|<e>,EEE,9,0
PART_WHOLE;;<e>,EEE,0,-7|arguments,NNS,0,-6|<e>,EEE,0,-5|locally,RB,1,-4|in,IN,2,-3|such,PDT,3,-2|a,DT,4,-1|<e>,EEE,5,0|sub-tree,JJ,6,0|<e>,EEE,7,0
RESULT;;<e>,EEE,0,-7|anchor,NN,0,-6|group,NN,0,-5|approach,NN,0,-4|<e>,EEE,0,-3|achieves,VBZ,1,-2|an,DT,2,-1|<e>,EEE,3,0|accuracy,NN,4,0|<e>,EEE,5,0
RESULT;;<e>,EEE,0,-6|prediction,NN,0,-5|of,IN,0,-4|MP,NN,0,-3|<e>,EEE,0,-2|improves,VBZ,1,-1|<e>,EEE,2,0|semantic,JJ,3,0|role,NN,4,0|labeling,NN,5,0|<e>,EEE,6,0
USAGE;;<e>,EEE,0,-8|technology,NN,0,-7|parsing,NN,0,-6|symbolic,JJ,0,-5|<e>,EEE,0,-4|a,DT,1,-3|on,IN,2,-2|based,VBN,3,-1|<e>,EEE,4,0|system,NN,5,0|translation,NN,6,0|multilingual,JJ,7,0|<e>,EEE,8,0
USAGE;;<e>,EEE,0,-6|learning,VBG,0,-5|weight,NN,0,-4|term,NN,0,-3|<e>,EEE,0,-2|using,VBG,1,-1|<e>,EEE,2,0|senses,NNS,3,0|word,NN,4,0|verbal,JJ,5,0|<e>,EEE,6,0
USAGE;;<e>,EEE,0,-9|parsers,NNS,0,-8|syntactic,NN,0,-7|statistical,JJ,0,-6|<e>,EEE,0,-5|full,JJ,1,-4|on,IN,2,-3|depend,VBP,3,-2|that,WDT,4,-1|<e>,EEE,5,0|chunking,VBG,6,0|<e>,EEE,7,0
COMPARE;;<e>,EEE,0,-8|phrase-by-phrase,NN,0,-7|approach,NN,0,-6|<e>,EEE,0,-5|performs,VBZ,1,-4|better,JJR,2,-3|than,IN,3,-2|its,PRP$,4,-1|<e>,EEE,5,0|word-by-word,JJ,6,0|counterpart,NN,7,0|<e>,EEE,8,0
USAGE;;<e>,EEE,0,-5|acoustic,JJ,0,-4|recognition,NN,0,-3|<e>,EEE,0,-2|of,IN,1,-1|<e>,EEE,2,0|continuous,JJ,3,0|speech,NN,4,0|<e>,EEE,5,0
USAGE;;<e>,EEE,0,-10|phonemes,NNS,0,-9|<e>,EEE,0,-8|and,CC,1,-7|their,PRP$,2,-6|coarticulation,NN,3,-5|for,IN,4,-4|the,DT,5,-3|purpose,NN,6,-2|of,IN,7,-1|<e>,EEE,8,0|large-vocabulary,JJ,9,0|continuous,JJ,10,0|speech,NN,11,0|recognition,NN,12,0|<e>,EEE,13,0
PART_WHOLE;;<e>,EEE,0,-13|word,NN,0,-12|list,NN,0,-11|<e>,EEE,0,-10|extracted,VBN,1,-9|from,IN,2,-8|what,WP,3,-7|has,VBZ,4,-6|been,VBN,5,-5|called,VBN,6,-4|',``,7,-3|the,DT,8,-2|study,NN,9,-1|<e>,EEE,10,0|corpus,NN,11,0|<e>,EEE,12,0
PART_WHOLE;;<e>,EEE,0,-7|word,NN,0,-6|list,NN,0,-5|<e>,EEE,0,-4|made,VBN,1,-3|from,IN,2,-2|a,DT,3,-1|<e>,EEE,4,0|reference,NN,5,0|corpus,NN,6,0|<e>,EEE,7,0
COMPARE;;<e>,EEE,0,-7|English,JJ,0,-6|corpora,NN,0,-5|<e>,EEE,0,-4|were,VBD,1,-3|compared,VBN,2,-2|to,TO,3,-1|<e>,EEE,4,0|reference,NN,5,0|corpora,NN,6,0|<e>,EEE,7,0
PART_WHOLE;;<e>,EEE,0,-8|keywords,NNS,0,-7|<e>,EEE,0,-6|of,IN,1,-5|number,NN,2,-4|larger,JJR,3,-3|a,DT,4,-2|yielded,VBD,5,-1|<e>,EEE,6,0|corpus,NN,7,0|<e>,EEE,8,0
COMPARE;;<e>,EEE,0,-14|reference,NN,0,-13|corpus,NN,0,-12|<e>,EEE,0,-11|that,WDT,1,-10|is,VBZ,2,-9|less,JJR,3,-8|than,IN,4,-7|five,CD,5,-6|times,NNS,6,-5|the,DT,7,-4|size,NN,8,-3|of,IN,9,-2|the,DT,10,-1|<e>,EEE,11,0|study,NN,12,0|corpus,NN,13,0|<e>,EEE,14,0
USAGE;;<e>,EEE,0,-5|dependency,NN,0,-4|analyser,NN,0,-3|<e>,EEE,0,-2|of,IN,1,-1|<e>,EEE,2,0|Italian,JJ,3,0|<e>,EEE,4,0
USAGE;;<e>,EEE,0,-5|lexical,JJ,0,-4|information,NN,0,-3|<e>,EEE,0,-2|to,TO,1,-1|<e>,EEE,2,0|parsing,NN,3,0|<e>,EEE,4,0
MODEL-FEATURE;;<e>,EEE,0,-6|relative,JJ,0,-5|informativeness,NN,0,-4|<e>,EEE,0,-3|of,IN,1,-2|a,DT,2,-1|<e>,EEE,3,0|word,NN,4,0|<e>,EEE,5,0
MODEL-FEATURE;;<e>,EEE,0,-4|measures,NNS,0,-3|<e>,EEE,0,-2|of,IN,1,-1|<e>,EEE,2,0|informativeness,NN,3,0|<e>,EEE,4,0
MODEL-FEATURE;;<e>,EEE,0,-5|informativeness,NN,0,-4|<e>,EEE,0,-3|of,IN,1,-2|a,DT,2,-1|<e>,EEE,3,0|word,NN,4,0|<e>,EEE,5,0