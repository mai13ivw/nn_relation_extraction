# Check single embeddings for the given features (POS and rel. positions)
# Test how similar some embeddings are, to find out what the model learned

from keras import backend as K
from core.loadModel import loadModel
from pprint import pprint
import numpy as np, sys, json
from scipy import spatial

with open('model_params.json') as f:
    model_params = json.load(f)

RESOURCES_FOLDER  = 'resources/'

domain = 'semEval'
task = '1'
name = '0107_011515'

# Load model
loaded_model_lstm = loadModel(name, domain, task, type='CNN')
print('Model loaded')

# Load POS-Tag IDs
pos_tags_dict = {}
with open(RESOURCES_FOLDER + 'posTags.txt') as f:
    tags = f.read().split(' ')
    for i, t in enumerate(tags):
        pos_tags_dict[t] = i

tags = [pos_tags_dict[tag] for tag in pos_tags_dict]
input = [0 for _ in range(0, 58)]
input[:len(tags)] = tags
print(input)

# Check layer
get_3rd_layer_output = K.function([loaded_model_lstm.layers[5].input],
                                  [loaded_model_lstm.layers[5].output])
pprint(vars(get_3rd_layer_output))
print(loaded_model_lstm.layers[5].input)

input = [[input]]
out = np.squeeze(get_3rd_layer_output(input), axis=2)

NN = out[pos_tags_dict['NN']]
NNP = out[pos_tags_dict['NNP']]
VB = out[pos_tags_dict['VB']]

#print(NN)
#print(NNP)
#print(VB)

for key in pos_tags_dict:
    tag = out[pos_tags_dict[key]]
    #print(1 - spatial.distance.cosine(NN, tag))

# NN NNP -> 0.9957056641578674
# NNPS

rel_positions_dict = {}
for i, t in enumerate(range(-model_params[domain]['max_sent_len'], model_params[domain]['max_sent_len'])):
    rel_positions_dict[str(t)] = i + 1
rel_positions_dict['NONE'] = len(rel_positions_dict) + 1

# indices: 1 to 117
input = [0 for _ in range(0, 58)]
for i in range(30):
    input[i] = rel_positions_dict[str(i)]
print(input)

# right relation
rel_output = K.function([loaded_model_lstm.layers[7].input],
                                  [loaded_model_lstm.layers[7].output])
pprint(vars(rel_output))
out = np.squeeze(rel_output([[input]]), axis=2)

pos0 = out[0]
for i in range(30):
    newPos = out[i]
    print('{}\t{}'.format(i, 1 - spatial.distance.cosine(newPos, pos0)))