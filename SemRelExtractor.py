# This is a test script for usecase scenarios
# You can use any sentence as input with relevant entities marked
# You have to decide on a domain from SemEval2018, SemEval2010, ScienceIE and BioNLP
# The script is going to filter all relevant relations 

# TODO still has some errors, that must be resolved

import os, sys, json, datetime, math, numpy as np
from random import randint
from core  import keras_models, io, score, labels as myLabels
from core.embeddings import embeddings
from core.loadModel import loadModel, loadModelsInFolder

from keras.models import model_from_json
from keras.callbacks import LearningRateScheduler
from predicting import task1, task2
from postprocessing import task2 as post2
from preprocessing.PreprocessSentence import preprocessMySentence

with open('model_params.json') as f:
    model_params = json.load(f)

RESOURCES_FOLDER  = 'resources/'
META_SPLIT        = ';;'

# Input sentence        
input = 'Recently the LATL has undertaken the development of a <e> multilingual translation system</e> based on a <e> symbolic parsing technology</e> and on a <e> transfer-based translation model</e>.'
possibleCombinations = preprocessMySentence(input)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--domain', default='semEval', choices=['semEval', 'BioNLP', 'semEval2010', 'scienceIE'])
    parser.add_argument('--amount', default=5)

    args    = parser.parse_args()
    domain  = args.domain
    amount  = int(args.amount)

    task    = '2'

    if(model_params[domain]['embedding'] == 50):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.50d.txt'
    elif(model_params[domain]['embedding'] == 100):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.100d.txt'
    elif(model_params[domain]['embedding'] == 200):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.200d.txt'
    elif(model_params[domain]['embedding'] == 300):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.300d.txt'
    elif(model_params[domain]['embedding'] == 0):
        WORD_EMB_PATH = RESOURCES_FOLDER + domain + '/embeddings/word_emb.txt'
    else:
        WORD_EMB_PATH = RESOURCES_FOLDER + 'easyGlove.txt'

    keras_models.initRelPositionDict(domain)
    
    ####################################################################################################
    # Save paths and labels
    if domain == 'semEval':
        modelsPath = RESOURCES_FOLDER + 'semEval/models/task_' + task
        label2idx = getattr(myLabels, "label2idx_semEval_" + task)
        idx2label  = {v: k for k, v in label2idx.items()}
    else:
        modelsPath = RESOURCES_FOLDER + domain + '/models/'
        label2idx = getattr(myLabels, "label2idx_" + domain)
        idx2label  = {v: k for k, v in label2idx.items()}
    
    ####################################################################################################
    # Load Word Embeddings and models
    word_emb_matrix, word2idx = embeddings.load(WORD_EMB_PATH)

    loaded_models_lstm = [None for i in range(0, amount)]
    loaded_models_cnn  = [None for i in range(0, amount)]

    loaded_models_cnn  = loadModelsInFolder(modelsPath, 'CNN', domain, amount, task)
    loaded_models_lstm = loadModelsInFolder(modelsPath, 'LSTM', domain, amount, task)

    combined_models = []
    for key in loaded_models_cnn:
        combined_models.append((key, loaded_models_cnn[key], loaded_models_lstm[key]))
    
    maxSent = model_params[domain]['max_sent_len']
    minSent = model_params[domain]['min_sent_len']

    predict_matrix = keras_models.to_indices(possibleCombinations, word2idx, label2idx, domain, predict=True) 
    print(predict_matrix)   
    predictions, probabilities, = task2.predictMany(predict_matrix[:-1], combined_models, maxSent, minSent, idx2label)
    print(predictions)
    if domain == 'semEval':
        predictions = post2.postProcessing([possibleCombinations], [predictions], [probabilities])
    print(predictions)
      
    for p, c in zip(predictions, possibleCombinations):
        line = ''
        for w in c.split('|'):
            line += w.split(',')[0] + ' '
        print("[{}] {}".format(p, line.strip()))