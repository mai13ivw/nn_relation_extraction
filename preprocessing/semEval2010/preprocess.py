import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import cleaning, positions, tagging, allDirections

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
resources = os.path.join(module_location, '../../resources/semEval2010/')

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('mode', choices=['clean', 'tagging', 'position', 'complete', 'allDirections'])
    parser.add_argument('--corpus', default='training', choices=['training', 'validation'])
    args = parser.parse_args()

    mode   = args.mode
    corpus = (args.corpus).lower()

    # Cleaning #
    if(mode == 'clean' or mode == 'complete'):
        if not os.path.exists(resources + 'preprocessed/'):
            os.makedirs(resources + 'preprocessed/')

        source_file = resources + 'raw/SemEval2010_task8_training/TRAIN_FILE.TXT'
        if corpus == 'validation':
            source_file = resources + 'raw/SemEval2010_task8_testing_keys/TEST_FILE_FULL.TXT'
        
        cleaning_file = resources + 'preprocessed/cleaning.' + corpus + '.txt'

        print('> Cleaning...')
        cleaning.clean(source_file, cleaning_file)
        print('> Saved to: \"{}\"'.format(cleaning_file))
        print()

    # Pos Tagging #
    if(mode == 'tagging' or mode == 'complete'):
        source_file = resources + 'preprocessed/cleaning.' + corpus + '.txt'
        pos_file    = resources + 'preprocessed/tagging.' + corpus + '.txt'

        print('> Pos Tagging...')
        tagging.createPOS(source_file, pos_file)
        print('> Saved to: \"{}\"'.format(pos_file))
        print()

    if((mode == 'allDirections' or mode == 'complete') and corpus == 'training'):
        # reverse directions and add to training data

        source_file = resources + 'preprocessed/tagging.training.txt'
        dest_file   = resources + 'preprocessed/directions.training.txt'

        print('> Add all directions Positions...')
        allDirections.addDirections(source_file, dest_file)
        print('> Saved to: \"{}\"'.format(dest_file))
        print()

    # Get relative positions for words in sentences #
    if(mode == 'position' or mode == 'complete'):
        if not os.path.exists(resources + 'preprocessed/corpora/'):
            os.makedirs(resources + 'preprocessed/corpora/')

        if corpus == 'training':
            source_file    = resources + 'preprocessed/tagging.training.txt'
        else:
            source_file    = resources + 'preprocessed/tagging.' + corpus + '.txt'
        positions_path = resources + 'preprocessed/corpora/' + corpus + '.txt'

        print('> Add relative Positions...')
        positions.add(source_file, positions_path)
        print('> Saved to: \"{}\"'.format(positions_path))
        print()