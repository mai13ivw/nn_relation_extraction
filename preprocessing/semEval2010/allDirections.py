from random import shuffle
META_SPLIT = ';;'

def getDirections(line):
    """Get reversed direction for the given line
    
    Args:
        param1 (str): line with relation

    Returns:
        list: two lines, one for each direction

    """
    newLines = [line]

    relation = line.split(META_SPLIT)[0]
    type = '_'.join(relation.split('_')[:-1])
    direction = relation.split('_')[-1]
    text = line.split(META_SPLIT)[-1]

    # only check for real relations
    if relation == 'NONE':
        return newLines

    newRelation = 'REVERSED' if(direction == 'NORMAL') else 'NORMAL'

    text = text.split('|')
    reversed_text = '|'.join(list(reversed(text)))
    reversed_text.strip('|')
    newLines.append(type + '_' + newRelation + ';;' + reversed_text)

    return newLines

def addDirections(source, dest):
    """Add directions to all relations

    Args:
        param1 (str): source path
        param2 (str): destination path
    
    """
    newLines = []

    with open(source, 'r') as fIn:
        for line in fIn:
            line = line.replace('\n', '')
            newLines.extend(getDirections(line))

    shuffle(newLines)
    
    with open(dest, 'w', encoding='utf-8') as fOut:
        for line in newLines:
            fOut.write(line + '\n')