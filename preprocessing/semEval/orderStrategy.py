# -*- coding: utf-8 -*-
# Only relevant for SemEval-2018 Task 1

META_SPLIT = ';;'
normal     = 'NORMAL'

def final_order(line):
    """Reverse word order, if a relation is REVERSED
    
    Args:
        param1 (str): line with words and relation type
        
    Returns:
        str: processed line. Do nothing if the type is NORMAL
        
    """
    split = line.split(META_SPLIT, 3)

    # Return immediately if relation is not reversed
    if(split[1] == normal):
        return (split[0] + META_SPLIT + split[2]).replace("\n", "")

    # If reversed, reverse text
    text = split[2].split("|")
    reversed_list = list(reversed(text))

    reversed_text = ''
    for part in reversed_list:
        reversed_text += part.replace("\n", "") + "|"

    return split[0] + META_SPLIT + reversed_text.strip('|')

def order(read, write):
    """Reverse all reversed relation types
    
    Args:
        param1 (str): source file path
        param2 (str): destination file path
        
    """
    with open(read, 'r') as fIn:
        with open(write, 'w', encoding='utf-8') as fOut:
            for line in fIn:
                fOut.write(final_order(line) + "\n")