# -*- coding: utf-8 -*-
import re

META_SPLIT      = ';;'
RESOURCES_PATH  = '../../resources/'

def split_into_sentences(text):
    """Deprecated function for sentence splitting

    Should be replaced by the setnence_splitter,
    But this could change too many results now

    Args:
        param1 (str): text

    Returns:
        list: splitted text

    .. Source:   
        https://stackoverflow.com/questions/4576077/python-split-text-on-sentences 
        I added an extra exception for float values and e.g. / i.e.
    
    """
    caps = "([A-Z])"
    prefixes = "(Mr|St|Mrs|Ms|Dr)[.]"
    suffixes = "(Inc|Ltd|Jr|Sr|Co)"
    starters = "(Mr|Mrs|Ms|Dr|He\s|She\s|It\s|They\s|Their\s|Our\s|We\s|But\s|However\s|That\s|This\s|Wherever|)"
    acronyms = "([A-Z][.][A-Z][.](?:[A-Z][.])?)"
    websites = "[.](com|net|org|io|gov)"

    digits = "([0-9])"
    text = " " + text + "  "
    text = text.replace("\n"," ")
    text = re.sub(prefixes,"\\1<prd>",text)
    text = re.sub(websites,"<prd>\\1",text)
    if "Ph.D" in text: text = text.replace("Ph.D.","Ph<prd>D<prd>")
    text = re.sub("\s" + caps + "[.] "," \\1<prd> ",text)
    text = re.sub(acronyms+" "+starters,"\\1<stop> \\2",text)
    text = re.sub(caps + "[.]" + caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>\\3<prd>",text)
    text = re.sub(caps + "[.]" + caps + "[.]","\\1<prd>\\2<prd>",text)
    text = re.sub(" "+suffixes+"[.] "+starters," \\1<stop> \\2",text)
    text = re.sub(" "+suffixes+"[.]"," \\1<prd>",text)
    text = re.sub(" " + caps + "[.]"," \\1<prd>",text)
    text = text.replace('e.g.', 'e<prd>g<prd>')
    text = text.replace('i.e.', 'i<prd>e<prd>')
    text = re.sub(digits + "[.]" + digits,"\\1<prd>\\2",text)
    if "”" in text: text = text.replace(".”","”.")
    if "\"" in text: text = text.replace(".\"","\".")
    if "!" in text: text = text.replace("!\"","\"!")
    if "?" in text: text = text.replace("?\"","\"?")
    text = text.replace(".",".<stop>")
    text = text.replace("?","?<stop>")
    text = text.replace("!","!<stop>")
    text = text.replace("<prd>",".")
    sentences = text.split("<stop>")
    sentences = sentences[:-1]
    sentences = [s.strip() for s in sentences]
    return sentences

def loadRelations(read):
    """Loads possible relations

    Args:
        param1 (str): source file path

    Returns:
        list: relations with entity1, entity2, direction and relation label
    
    """
    with open(read, 'r', encoding='utf-8') as fIn:
        relations = []
        for line in fIn:
            line = line.split('(')
            rel = line[0]
            entities = line[-1].replace(')\n', '').split(',')

            type = 'NORMAL'
            if(len(entities) == 3):
                type = 'REVERSED'
            tuple = (rel + META_SPLIT + type, entities[0], entities[1])
            relations.append(tuple)
        return relations

def cropAbstract(sentences):
    """For all sentences receive all entities, that could have a relation

    Args:
        param1 (list): all sentences

    Returns:
        list: tuples of sentences and their entity combinations
        >>> print(cropAbstract([mySentence]))
        (mySentence, [(e1, e2), (e1, e3), ...])
    
    """
    startSequence = "<entity id=\""
    processed_sentences = []

    for sentence in sentences:
        pos = 0
        ids = []
        pairs = []

        # get all entity ids per sentence
        while(pos != -1):
            pos = sentence.find(startSequence, pos)
            if(pos != -1):
                pos = pos + len(startSequence)
                pos2 = sentence.find("\">", pos)
                ids.append(sentence[pos:pos2])
                pos = pos2
        
        # pair all entity ids with each other #
        if len(ids) > 1:
            for i, e1 in enumerate(ids):
                for j, e2 in enumerate(ids):
                    if j > i:
                        pairs.append((e1, e2))

        
        # Check if number of found pairs matches the number of ids
        N = (len(ids) ** 2 - len(ids)) / 2
        assert(N == len(pairs))

        tuple = (sentence, pairs)
        processed_sentences.append(tuple)
    return processed_sentences

def createSaveData(sentence, data):
    """Create a savable format for a sentence

    Each sentence can have multiple relations,
    For all relations both entities, context and the relation type must be saved

    Args:
        param1 (str): sentence
        param2 (list): all relations in this sentence (inclusive NONE) with entities
        
    Returns:
        list: all relations as str lines for one sentence
        
    """
    start = '<entity id="'
    end = '</entity>'
    lines = []

    for relation in data:
        id1 = relation[1][0]
        id2 = relation[1][1]
        rel = relation[0]

        pos1 = sentence.find(start + id1 + '">')
        pos2 = sentence.find(start + id2 + '">')
        pos2 = sentence.find(end, pos2) + len(end)
        line = rel + META_SPLIT + sentence[pos1:pos2]
        lines.append(line)
    return lines

def relationOfPair(relations, pair):
    """Check what relation the pair of entities has

    Args:
        param1 (list): All relations the file contains (Gold Standard)
        param2 (tuple): Two entities, that could have a relation

    Returns:
        str: relation type of both entities

    """
    for relation in relations:
        if(relation[1] == pair[0] and relation[2] == pair[1]):
            return relation[0]
    return 'NONE'

def croppingSentences(sources, write):
    """Crop a text file with all sentences and relations relations,

    The output file has the same format as for subtask 1,
    However sentences are split by line breaks
    
    Args:
        param1 (list): multiple sources with relations and XML-files
        >>> mySources = [[xml1, rel1], [xml2, rel2], ...]
        param2 (str): file to write the concatenated information to

    """
    with open(write, 'w', encoding='utf-8') as fOut:
        files = 1
        for source in sources:
            with open(source[0], 'r', encoding='utf-8') as fIn:
                text = fIn.read()

            abstracts = []
            # get an array with all abstracts #
            for item in text.split("</abstract>"):
                if "<abstract>" in item:
                    abstract = item[item.find("<abstract>")+len("<abstract>"):]
                    abstract = abstract.replace('\n', '')
                    abstracts.append(abstract)

            # load correct relations #
            relations = loadRelations(source[1])
            
            for abstract in abstracts:
                # check for all sentences in the abstract
                sentences = split_into_sentences(abstract)
                sentences = cropAbstract(sentences)

                for sentence in sentences:
                    all_rels = []
                    entity_pairs = sentence[-1]

                    # check for relations of each entity pair. for no relation use NONE #
                    for pair in entity_pairs:
                        tuple = (relationOfPair(relations, pair), pair)
                        all_rels.append(tuple)

                    # find entity pairs in sentence, create finished lines
                    lines = createSaveData(sentence[0], all_rels)

                    for line in lines:
                        if source[2] == 1:
                            if line.split(';;')[0] == 'NONE':
                                continue
                        fOut.write(line + '\n')

                    if source[2] != 1:
                        if len(lines) > 0:
                            fOut.write('\n')
            print("\t> File {} finished".format(files))
            files += 1
