# -*- coding: utf-8 -*-
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import fromstring, ElementTree, Element, tostring
from lxml import etree

def flatten(files):
    """Flatten XML-files

    If there are entities that contain other entities, delete inner tags

    Args:
        param1 (list): all files and destinations, [[xml1, dest1], [xml2, dest2], ...]

    """
    count = 1
    for file in files:
        read = file[0]
        write = file[1]

        tree = ET.parse(read)
        for node in tree.iter('entity'):
            # Check for node without any text and children
            if(node.text == None):
                text = ''
                for node2 in node.findall('entity'):
                    text += node2.text + ' '
                for node2 in node.findall('entity'):
                    node.remove(node2)
                text = text.strip()
                node.text = text

            # Check for node with text and children
            s = sum(1 for node2 in node.findall('entity'))
            if(s > 0):
                text = node.text
                for node2 in node.findall('entity'):
                    text += node2.text + ' '
                    node.remove(node2)
                text = text.strip()
                node.text = text
        
        tree.write(open(write, 'wb'))
        print("\t> File {} finished".format(count))
        count += 1