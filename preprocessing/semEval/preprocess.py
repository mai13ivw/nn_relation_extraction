# -*- coding: utf-8 -*-
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
import flattenXML, cropping1, cropping2, cleaning1, cleaning2, orderStrategy, positions, tagging

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
resources = os.path.join(module_location, '../../resources/semEval/')

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('mode', choices=['flatten', 'crop', 'clean', 'tagging', 'order', 'position', 'complete'])
    parser.add_argument('--corpus', default='training', choices=['training', 'validation'])
    parser.add_argument('--task', default='1', choices=['1', '2'])
    args = parser.parse_args()

    mode   = args.mode
    task   = int(args.task)
    corpus = (args.corpus).lower()

    # Flatten #
    if(mode == 'flatten'):
        if not os.path.exists(resources + 'flatXML/'):
            os.makedirs(resources + 'flatXML/')

        files = [
            [resources + 'raw/1.1.text.xml', resources + 'flatXML/1.1.text.xml'],
            [resources + 'raw/1.2.text.xml', resources + 'flatXML/1.2.text.xml'],
            [resources + 'raw/1.1.test.text.xml', resources + 'flatXML/1.1.test.text.xml'],
            [resources + 'raw/1.2.test.text.xml', resources + 'flatXML/1.2.test.text.xml'],
            [resources + 'raw/2.test.text.xml', resources + 'flatXML/2.test.text.xml']
        ]

        print('> Flattening...')
        flattenXML.flatten(files)
        for f in files:
            print('> Saved to: \"{}\"'.format(f[1]))
        print()

    # Cropping #
    if(mode == 'crop' or mode == 'complete'):
        if not os.path.exists(resources + 'preprocessed/'):
            os.makedirs(resources + 'preprocessed/')

        if corpus == 'training':
            name_1_1 = '1.1.text.xml'
            name_1_2 = '1.2.text.xml'
            rel_1_1  = '1.1.relations.txt'
            rel_1_2  = '1.2.relations.txt'
        elif corpus == 'validation':
            name_1_1 = '1.1.test.text.xml'
            name_1_2 = '1.2.test.text.xml'
            rel_1_1  = 'keys.test.1.1.txt'
            rel_1_2  = 'keys.test.1.2.txt'

        if task == 1:
            cropping_file = resources + 'preprocessed/1.cropping.' + corpus + '.txt'
            sources = [
                [resources + 'flatXML/' + name_1_1, resources + 'raw/' + rel_1_1, 0],
                [resources + 'flatXML/' + name_1_2, resources + 'raw/' + rel_1_2, 0]
            ]
        elif task == 2:
            cropping_file = resources + 'preprocessed/2.cropping.' + corpus + '.txt'

            if corpus == 'validation':
                name_1_1 = '2.test.text.xml'
                rel_1_1  = 'keys.test.2.txt'

            sources = [
                [resources + 'flatXML/' + name_1_1, resources + 'raw/' + rel_1_1, 0],
                [resources + 'flatXML/' + name_1_2, resources + 'raw/' + rel_1_2, 0]
            ]

        print('> Cropping...')
        if task == 1:
            cropping1.croppingSentences(sources, cropping_file)
        elif task == 2:
            # add the files created by subtask 1 #
            if corpus == 'training':
                sources.append([resources + 'flatXML/1.1.test.text.xml', resources + 'predicted/keys.pred.1.1.txt', 1])
                sources.append([resources + 'flatXML/1.2.test.text.xml', resources + 'predicted/keys.pred.1.2.txt', 1])
            cropping2.croppingSentences(sources, cropping_file)
        print('> Saved to: \"{}\"'.format(cropping_file))
        print()

    # Cleaning #
    if(mode == 'clean' or mode == 'complete'):
        if task == 1:
            source_file   = resources + 'preprocessed/1.cropping.' + corpus + '.txt'
            cleaning_file = resources + 'preprocessed/1.cleaning.' + corpus + '.txt'
        elif task == 2:
            source_file     = resources + 'preprocessed/2.cropping.' + corpus + '.txt'
            cleaning_file   = resources + 'preprocessed/2.cleaning.' + corpus + '.txt'
            cleaning_file_2 = resources + 'preprocessed/2.cleaning2.' + corpus + '.txt'

        print('> Cleaning...')
        cleaning1.clean(source_file, cleaning_file)
        print('> Saved to: \"{}\"'.format(cleaning_file))

        if task == 2:
            cleaning2.clean(cleaning_file, cleaning_file_2)
            print('> Saved to: \"{}\"'.format(cleaning_file_2))
        print()

    # Pos Tagging #
    if(mode == 'tagging' or mode == 'complete'):
        if task == 1:
            source_file   = resources + 'preprocessed/1.cleaning.' + corpus + '.txt'
            pos_file      = resources + 'preprocessed/1.pos_tagging.' + corpus + '.txt'
        elif task == 2:
            source_file   = resources + 'preprocessed/2.cleaning2.' + corpus + '.txt'
            pos_file      = resources + 'preprocessed/2.pos_tagging.' + corpus + '.txt'

        print('> Pos Tagging...')
        tagging.createPOS(source_file, pos_file)
        print('> Saved to: \"{}\"'.format(pos_file))
        print()

    # Order Strategy for reversed relations, only for first task relevant #
    if(mode == 'order' or mode == 'complete'):
        if task == 1:
            source_file   = resources + 'preprocessed/1.pos_tagging.' + corpus + '.txt'
            order_path    = resources + 'preprocessed/1.order_strategy.' + corpus + '.txt'

            print('> Reverse all reversed relations...')
            orderStrategy.order(source_file, order_path)
            print('> Saved to: \"{}\"'.format(order_path))
            print()

    # Get relative positions for words in sentences #
    if(mode == 'position' or mode == 'complete'):
        if not os.path.exists(resources + 'preprocessed/corpora/'):
            os.makedirs(resources + 'preprocessed/corpora/')

        if task == 1:
            source_file    = resources + 'preprocessed/1.order_strategy.' + corpus + '.txt'
            positions_path = resources + 'preprocessed/corpora/1.' + corpus + '.txt'
        elif task == 2:
            source_file    = resources + 'preprocessed/2.pos_tagging.' + corpus + '.txt'
            positions_path = resources + 'preprocessed/corpora/2.' + corpus + '.txt'

        print('> Add relative Positions...')
        positions.add(source_file, positions_path)
        print('> Saved to: \"{}\"'.format(positions_path))
        print()