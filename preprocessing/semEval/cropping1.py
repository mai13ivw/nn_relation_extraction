# -*- coding: utf-8 -*-
from lxml import etree
import re

META_SPLIT = ';;'
reversed   = 'REVERSED'
normal     = 'NORMAL'

def addRelation(line, relations):
    """Clean lines from relations.txt, create data list
    
    Args:
        param1 (str): line that contains a relation
        param2 (list): all relations, used as storage
        
    """
    split = line.split('(')
    rel_type = split[0]
    rel_entities = split[1].replace('\n', '').replace(')', '')
    rel_entities = rel_entities.split(',')
    relations.append([rel_type, rel_entities])

def removeInnerTags(line):
    """Remove middle Tags with XML-named tags

    Reference:
        cleaning2.removeInnerTags

    Args:
        param1 (str): input line

    Returns:
        str: line without inner tags

    """
    endTag = '</entity>'
    start = line.find(endTag) + len(endTag)
    end = line.rfind('<entity')
    middle = line[start:end]
    middle = middle.replace('</entity>', '')
    middle = re.sub(r'<entity id=\".*\">', '', middle)
    return line[:start] + middle + line[end:]

def croppingSentences(sources, write):
    """Create a file with all Entities that have a relation and text between as context

    Args:
        param1 (list): multiple sources with relations and XML-files
        >>> mySources = [[xml1, rel1], [xml2, rel2], ...]
        param2 (str): file to write the concatenated information to

    """
    with open(write, 'w', encoding='utf-8') as f:
        files = 1
        for source in sources:
            # initialize values
            relations = []

            # read as xml tree
            with open(source[0], 'r', encoding='utf-8') as f_xml:
                tree = etree.parse(f_xml)

            # read as normal text file
            with open(source[0], 'r', encoding='utf-8') as f_text:
                text = f_text.read()

            # load relations
            with open(source[1], 'r', encoding='utf-8') as f_rel:
                for line in f_rel:
                    addRelation(line, relations)

            root = tree.getroot()

            # every relation exists in the xml file, look for them and save them
            for rel in relations:
                r2 = root.xpath("//entity[@id = '%s']" % rel[1][1])[0]
                try:
                    idx1 = text.index(rel[1][0])
                    idx2 = text.index(rel[1][1])
                    direction = normal
                    if(len(rel[1]) > 2):
                        direction = reversed

                    sequence = rel[0] + META_SPLIT + direction + META_SPLIT + '<entity id=\"' + text[idx1:idx2] + rel[1][1] + '\">' + r2.text + "</entity>"
                    sequence = removeInnerTags(sequence)
                    f.write(sequence + '\n')
                except ValueError:
                    pass
            print('\t> File {} finished'.format(files))
            files += 1