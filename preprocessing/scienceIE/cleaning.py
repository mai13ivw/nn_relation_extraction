import os, re, sys
from sentence_splitter import SentenceSplitter, split_text_into_sentences
module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)

splitter = SentenceSplitter(language='en')
RESOURCES_FOLDER = os.path.join(module_location, '../../resources/')

PUNCTUATION = '"#$%&\'()*,/=@[\\]^`{|}~'

def getFileNames(dir):
    """ Get file names in a directory
    
    Args:
        param1 (str): directory path
        
    Returns:
        list: all file names without extension
        
    """
    names = set([])
    for filename in os.listdir(dir):
        names.add('.'.join(filename.split('.')[:-1]))
    return names

def createT(splits):
    """Create an entity

    This entity contains all given data from the input file

    Args:
        param1 (list): splittet information
    
    Return:
        tuple: ordered data
    
    """
    data = splits[1].split(" ")
    type = data[0]
    pos1 = int(data[1])
    pos2 = int(data[-1])
    return (type, pos1, pos2)

def createR(splits):
    """Create an HYPERNYM-Relation with IDs of two given entities

    Args:
        param1 (list): splittet information
    
    Return:
        tuple: ordered data
    
    """
    data = splits[-2].strip()
    data2 = data.split(" ")
    rel = data2[0]
    e1 = data2[1].split(":")[-1]
    e2 = data2[2].split(":")[-1]
    return (rel, e1, e2)

def createS(splits):
    """Create an SYNONYM-Relation with IDs of two given entities

    Args:
        param1 (list): splittet information
    
    Return:
        tuple: ordered data
    
    """

    data = splits[-1].strip()
    data2 = data.split(" ")
    rel = data2[0]
    e1 = data2[1]
    e2 = data2[2]
    return (rel, e1, e2)

def cleanLine(line):
    """Remove punctuation and reset . ! and ?

    Args:
        param1 (str): line that must be cleaned

    Returns:
        str: cleaned line
    
    """
    words = line.split()
    table = str.maketrans('', '', PUNCTUATION)
    stripped = [w.translate(table).strip() for w in words if w != '']
    line = ' '.join(stripped)
    return line.replace(" .", ".").replace(" ?", "?").replace(" !", "!").strip()

def addSentenceIdToEntities(entities, sentences):
    """Set specific sentence ID to each entitiy

    This is important to check, if two entities can be related

    Args:
        param1 (list): all entities
        param2 (list): all proteins
        param3 (list): all sentences
    
    """
    sentenceSpaces = []
    pointer = 0

    for sentence in sentences:
        newPointer = pointer + len(sentence)
        sentenceSpaces.append((pointer, newPointer))
        pointer = newPointer+1
    
    for entity in entities:
        e_data = entities[entity]
        sent   = 0
        for i, sentenceSpace in enumerate(sentenceSpaces):
            if e_data[1] > sentenceSpace[0] and e_data[2] < sentenceSpace[1]:
                sent = i
                break
        entities[entity] = (e_data[0], e_data[1], e_data[2], sent)

def getSentences(dir, fileNames, dest):
    """Create a cleaned file out of the ScienceIE data sets

    Args:
        param1 (str): data directory
        param2 (list): all file names in the directory
        param3 (str): destination to save the cleaned data set

    """ 
    with open(dest, 'w', encoding='utf8') as fOut:
        relationSum = 0
        for name in fileNames:
            relations = []
            entities  = {}

            # read all sentences for this document
            with open(dir + name + '.txt', 'r', encoding='utf8') as fIn:
                text = fIn.read()
                sentences = splitter.split(text=text)

            # read all relations and entities for this document
            try:
                with open(dir + name + '.ann', 'r', encoding='utf8') as fIn:
                    for line in fIn:
                        splits = line.split('\t')
                        if 'R' in splits[0]:
                            # Hyponym relation
                            relations.append(createR(splits))
                        if '*' in splits[0]:
                            relations.append(createS(splits))
                        elif 'T' in splits[0]:
                            entities[splits[0]] = createT(splits)
            except IOError:
                pass

            relationSum += len(relations)
            
            # add sentence number to all entities
            addSentenceIdToEntities(entities, sentences)

            # try out all possible combinations #
            for i, entity1 in enumerate(entities):
                for j, entity2 in enumerate(entities):
                    try:
                        e1_data = entities[entity1]
                        e2_data = entities[entity2]

                        if j <= i or e1_data[3] != e2_data[3]:
                            continue

                        startPos1 = e1_data[1]
                        endPos1   = e1_data[2]
                        startPos2 = e2_data[1]
                        endPos2   = e2_data[2]
                        type = "NORMAL"

                        if startPos1 > endPos2:
                            startPos1 = e2_data[1]
                            endPos1   = e2_data[2]
                            startPos2 = e1_data[1]
                            endPos2   = e1_data[2]
                            type = "REVERSED"
                        
                        e1     = text[startPos1:endPos1].strip()
                        middle = text[endPos1:startPos2].strip()
                        e2     = text[startPos2:endPos2].strip()
                        newText = "<e> " + e1 + " <e> " + middle + " <e> " + e2 + " <e>"
                        newText = ' '.join(newText.split())

                        # does the given relation exist? #
                        foundRel = None
                        for rel in relations:
                            if (entity1 == rel[1] and entity2 == rel[2]) or (entity1 == rel[2] and entity2 == rel[1]):
                                foundRel = rel
                                break
                        if foundRel:
                            if foundRel[0] != 'Synonym-of':
                                newText = foundRel[0] + "_" + type + ";;" + newText
                            else:
                                newText = foundRel[0] + ";;" + newText
                            fOut.write(cleanLine(newText) + "\n")
                        else:
                            if len(middle.split()) < 9:
                                newText = "NONE;;" + newText
                                fOut.write(cleanLine(newText) + "\n")
                    except:
                        pass
            fOut.write('\n')