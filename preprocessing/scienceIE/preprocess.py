# -*- coding: utf-8 -*-
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 

import sys, os
import cleaning, tagging, positions

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
resources = os.path.join(module_location, '../../resources/')

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('mode', choices=['clean', 'tagging', 'position', 'split', 'complete'])
    parser.add_argument('--corpus', default='training', choices=['training', 'validation'])
    args = parser.parse_args()
    mode   = args.mode
    corpus = args.corpus

    # Cleaning #
    if(mode == 'clean' or mode == 'complete'):

        if corpus == 'training':
            source_file = resources + '/scienceIE/raw/scienceie2017_train/train2/'
        elif corpus == 'validation':
            source_file = resources + '/scienceIE/raw/semeval_articles_test/'

        cleaning_file = resources + '/scienceIE/preprocessed/cleaning.' + corpus + '.txt'

        if not os.path.exists(resources + '/scienceIE/preprocessed/'):
            os.makedirs(resources + '/scienceIE/preprocessed/')

        print('> Cleaning...')
        filenames = cleaning.getFileNames(source_file)
        cleaning.getSentences(source_file, filenames, cleaning_file)
        print('> Saved to: \"{}\"'.format(cleaning_file))
        print()

    # Pos Tagging #
    if(mode == 'tagging' or mode == 'complete'):
        source_file = resources + '/scienceIE/preprocessed/cleaning.' + corpus + '.txt'
        pos_file    = resources + '/scienceIE/preprocessed/tagging.' + corpus + '.txt'

        print('> Pos Tagging...')
        tagging.createPOS(source_file, pos_file)
        print('> Saved to: \"{}\"'.format(pos_file))
        print()

    # Get relative positions for words in sentences #
    if(mode == 'position' or mode == 'complete'):
        if not os.path.exists(resources + '/scienceIE/preprocessed/corpora/'):
            os.makedirs(resources + '/scienceIE/preprocessed/corpora/')

        source_file    = resources + '/scienceIE/preprocessed/tagging.' + corpus + '.txt'
        positions_path = resources + '/scienceIE/preprocessed/corpora/' + corpus + '.txt'

        print('> Add relative Positions...')
        positions.add(source_file, positions_path)
        print('> Saved to: \"{}\"'.format(positions_path))
        print()