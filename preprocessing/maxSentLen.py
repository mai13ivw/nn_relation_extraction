# -*- coding: utf-8 -*-
import re, os, unittest

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
RESOURCES_FOLDER = os.path.join(module_location, '../resources/')
META_SPLIT = ';;'

def get_sent_lens(path):
    """Get min and max sentence lengths for a given file
    
    Args:
        param1 (str): Path to file

    Returns:
        int, int: max sentence length and min sentence length

    """
    max_len = -1
    min_len = 1000000000
    with open(path, 'r', encoding='utf8') as fIn:
        for line in fIn:
            if line.strip() == '':
                continue
            sentence = (line.split(META_SPLIT))[-1]
            sentence = sentence.replace('<e>', '')
            token_size = len(sentence.split())
            if token_size > max_len:
                max_len = token_size
                print("New MAX: " + line)
            if token_size < min_len:
                min_len = token_size
                print("New MIN: " + line)
    return max_len, min_len


if __name__ == '__main__':
    import argparse

    # Load Args Parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('domain', default='semEval', choices=['semEval', 'semEval2010', 'BioNLP', 'scienceIE'])
    args = parser.parse_args()
    domain = args.domain

    if domain == 'semEval':
        train_path = RESOURCES_FOLDER + domain + '/preprocessed/1.cleaning.training.txt'
        val_path   = RESOURCES_FOLDER + domain + '/preprocessed/1.cleaning.validation.txt'

        train_max_len, train_min_len = get_sent_lens(train_path)
        val_max_len, val_min_len  = get_sent_lens(val_path)

        # Relations always inside sentences
        max_len = max(train_max_len, val_max_len) # = 29
        min_len = min(train_min_len, val_min_len) # = 2

    if domain == 'BioNLP':
        train_path = RESOURCES_FOLDER + domain + '/preprocessed/cleaning.txt'
        train_max_len, train_min_len = get_sent_lens(train_path)

        # Relations can go beyond sentences
        max_len = train_max_len # = 113
        min_len = train_min_len # = 2

    if domain == 'semEval2010':
        train_path = RESOURCES_FOLDER + domain + '/preprocessed/cleaning.training.txt'
        train_max_len, train_min_len = get_sent_lens(train_path)

        # Relations can go beyond sentences
        max_len = train_max_len # = 32
        min_len = train_min_len # = 2

    if domain == 'scienceIE':
        train_path = RESOURCES_FOLDER + domain + '/preprocessed/cleaning.training.txt'
        train_max_len, train_min_len = get_sent_lens(train_path)

        # Relations can go beyond sentences
        max_len = train_max_len # = 98 (Mit fehlerhaftem '.' der vom sentence_splitter ignoriert wird. Korrekt wäre 87)
        min_len = train_min_len # = 2
    
    print("Longest sentence length: {}".format(max_len))
    print("Shortest sentence length: {}".format(min_len))