# -*- coding: utf-8 -*-
import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir) 
module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)

import cleaning1, tagging, positions

resources = os.path.join(module_location, '../../resources/')

if __name__ == '__main__':
    # Preprocess the BioNLP data set
    # Set mode to 'complete' to do everything in one go
    # Or do everything step by step (keep the given order in mind!)

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('mode', choices=['clean', 'tagging', 'position', 'split', 'complete'])
    args = parser.parse_args()
    mode   = args.mode

    # Cleaning #
    if(mode == 'clean' or mode == 'complete'):
        source_file = resources + '/BioNLP/raw/BioNLP-ST_2011_Entity_Relations_training_data/'
        cleaning_file = resources + '/BioNLP/preprocessed/cleaning.txt'

        if not os.path.exists(resources + '/BioNLP/preprocessed/'):
            os.makedirs(resources + '/BioNLP/preprocessed/')

        print('> Cleaning...')
        filenames = cleaning1.getFileNames(source_file)
        cleaning1.getSentencesWithoutProteins(source_file, filenames, cleaning_file)
        print('> Saved to: \"{}\"'.format(cleaning_file))
        print()

    # Pos Tagging #
    if(mode == 'tagging' or mode == 'complete'):
        source_file = resources + '/BioNLP/preprocessed/cleaning.txt'
        pos_file    = resources + '/BioNLP/preprocessed/tagging.txt'

        print('> Pos Tagging...')
        tagging.createPOS(source_file, pos_file)
        print('> Saved to: \"{}\"'.format(pos_file))
        print()

    # Get relative positions for words in sentences #
    if(mode == 'position' or mode == 'complete'):
        if not os.path.exists(resources + '/BioNLP/preprocessed/corpora/'):
            os.makedirs(resources + '/BioNLP/preprocessed/corpora/')

        source_file    = resources + '/BioNLP/preprocessed/tagging.txt'
        positions_path = resources + '/BioNLP/preprocessed/corpora/corpus.txt'

        print('> Add relative Positions...')
        positions.add(source_file, positions_path)
        print('> Saved to: \"{}\"'.format(positions_path))
        print()

    # Split data to training and validation set #
    if mode == 'split' or mode == 'complete':
        training_size   = 0.75
        corpus_file     = resources + '/BioNLP/preprocessed/corpora/corpus.txt'
        training_file   = resources + '/BioNLP/preprocessed/corpora/training.txt'
        validation_file = resources + '/BioNLP/preprocessed/corpora/validation.txt'
        documents       = 0

        with open(corpus_file, 'r') as fIn:
            for line in fIn:
                if line.strip() == '':
                    documents += 1
        
        training_batch = (int) (training_size * documents)
        validation_batch = documents - training_batch

        print("Number of documents: {}".format(documents))
        print("Samples for Training: {}".format(training_batch))
        print("Samples for Validation: {}".format(validation_batch))

        documents = 0
        with open(training_file, 'w') as fTrain:
            with open(validation_file, 'w') as fVal:
                with open(corpus_file, 'r') as fIn:
                    for line in fIn:
                        if documents < training_batch:
                            fTrain.write(line)
                        else:
                            fVal.write(line)
                        if line.strip() == '':
                            documents += 1