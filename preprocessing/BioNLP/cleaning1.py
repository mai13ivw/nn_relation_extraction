import os, re, sys
from sentence_splitter import SentenceSplitter, split_text_into_sentences

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
RESOURCES_FOLDER = os.path.join(module_location, '../../resources/')

PUNCTUATION = '"#$%&\'()*,/=@[\\]^`{|}~'
splitter = SentenceSplitter(language='en')

def getFileNames(dir):
    """Get all file names in directory except README and LICENSE

    Args:
        param1 (str): directory path
    
    Return:
        list: all file names
    
    """
    names = set([])
    for filename in os.listdir(dir):
        if 'LICENSE' not in filename and 'README' not in filename:
            names.add('.'.join(filename.split('.')[:-1]))
    return names

def createT(splits):
    """Create an entity

    This entity contains all given data from the input file

    Args:
        param1 (list): splittet information
    
    Return:
        tuple: ordered data
    
    """
    data = splits[1].split(" ")
    type = data[0]
    pos1 = int(data[1])
    pos2 = int(data[2])
    return (type, pos1, pos2)

def createR(splits):
    """Create an relation with IDs of two given entities

    Args:
        param1 (list): splittet information
    
    Return:
        tuple: ordered data
    
    """
    data = splits[-1].strip()
    data2 = data.split(" ")
    rel = data2[0]
    e1 = data2[1].split(":")[-1]
    e2 = data2[2].split(":")[-1]
    return (rel, e1, e2)

def cleanLine(line):
    """Remove punctuation and reset . ! and ?

    Args:
        param1 (str): line that must be cleaned

    Returns:
        str: cleaned line
    
    """
    words = line.split()
    table = str.maketrans('', '', PUNCTUATION)
    stripped = [w.translate(table).strip() for w in words if w != '']
    line = ' '.join(stripped)
    return line.replace(" .", ".").replace(" ?", "?").replace(" !", "!").strip()

def addSentenceIdToEntities(entities, proteins, sentences):
    """Set specific sentence ID to each entitiy

    This is important to check, if two entities can be related

    Args:
        param1 (list): all entities
        param2 (list): all proteins
        param3 (list): all sentences
    
    """
    sentenceSpaces = []
    pointer = 0

    for sentence in sentences:
        newPointer = pointer + len(sentence)
        sentenceSpaces.append((pointer, newPointer))
        pointer = newPointer+1
    
    for entity in entities:
        e_data = entities[entity]
        sent   = 0
        for i, sentenceSpace in enumerate(sentenceSpaces):
            if e_data[1] >= sentenceSpace[0] and e_data[2] <= sentenceSpace[1]:
                sent = i
                break
        entities[entity] = (e_data[0], e_data[1], e_data[2], sent)

    if proteins:
        for protein in proteins:
            e_data = proteins[protein]
            sent   = 0
            for i, sentenceSpace in enumerate(sentenceSpaces):
                if e_data[1] >= sentenceSpace[0] and e_data[2] <= sentenceSpace[1]:
                    sent = i
                    break
            proteins[protein] = (e_data[0], e_data[1], e_data[2], sent)


def getSentences(dir, fileNames, dest):
    """Create a cleaned file out of the BioNLP data sets

    Args:
        param1 (str): data directory
        param2 (list): all file names in the directory
        param3 (str): destination to save the cleaned data set

    """
    fileNames = sorted(list(fileNames))
    print(fileNames[:10])
    with open(dest, 'w') as fOut:
        for name in fileNames:
            relations = []
            entities  = {}
            proteins  = {}

            with open(dir + name + '.txt', 'r') as fIn:
                text = fIn.read()
                sentences = splitter.split(text=text)
            try:
                with open(dir + name + '.rel', 'r') as fIn:
                    for line in fIn:
                        splits = line.split('\t')
                        if 'R' in splits[0]:
                            relations.append(createR(splits))
                        elif 'T' in splits[0]:
                            entities[splits[0]] = createT(splits)
                if len(relations):
                    try:
                        with open(dir + name + '.a1', 'r') as fIn:
                            for line in fIn:
                                splits = line.split('\t')
                                if 'T' in splits[0]:
                                    proteins[splits[0]] = createT(splits)
                    except IOError:
                        pass
                else:
                    continue
            except IOError:
                continue

            # add sentence number to all entities
            addSentenceIdToEntities(entities, proteins, sentences)
            
            # try out all possible combinations #
            for protein in proteins:
                for entity in entities:
                    try:
                        p_data = proteins[protein]
                        e_data = entities[entity]

                        if p_data[3] != e_data[3]:
                            continue

                        startPos1 = p_data[1]
                        endPos1   = p_data[2]
                        startPos2 = e_data[1]
                        endPos2   = e_data[2]

                        if startPos1 > endPos2:
                            startPos1 = e_data[1]
                            endPos1   = e_data[2]
                            startPos2 = p_data[1]
                            endPos2   = p_data[2]
                        
                        e1     = text[startPos1:endPos1].strip()
                        middle = text[endPos1:startPos2].strip()
                        e2     = text[startPos2:endPos2].strip()
                        newText = "<e> " + e1 + " <e> " + middle + " <e> " + e2 + " <e>"
                        newText = ' '.join(newText.split())

                        # does the given relation exist? #
                        foundRel = None

                        for rel in relations:
                            if (protein == rel[1] and entity == rel[2]):
                                foundRel = rel
                                break

                        if foundRel:
                            newText = foundRel[0] + ";;" + newText
                            fOut.write(cleanLine(newText) + "\n")
                        else:
                            newText = "NONE;;" + newText
                            fOut.write(cleanLine(newText) + "\n")
                    except:
                        pass
            fOut.write('\n')

def getSentencesWithoutProteins(dir, fileNames, dest):
    """Same as getSentences, however proteins and entities aren't separated

    Args:
        param1 (str): data directory
        param2 (list): all file names in the directory
        param3 (str): destination to save the cleaned data set

    """
    fileNames = sorted(list(fileNames))
    print(fileNames[:10])
    with open(dest, 'w') as fOut:
        for name in fileNames:
            relations = []
            entities  = {}
            proteins  = {}

            with open(dir + name + '.txt', 'r') as fIn:
                text = fIn.read()
                sentences = splitter.split(text=text)
            try:
                with open(dir + name + '.rel', 'r') as fIn:
                    for line in fIn:
                        splits = line.split('\t')
                        if 'R' in splits[0]:
                            relations.append(createR(splits))
                        elif 'T' in splits[0]:
                            entities[splits[0]] = createT(splits)
                if len(relations):
                    try:
                        with open(dir + name + '.a1', 'r') as fIn:
                            for line in fIn:
                                splits = line.split('\t')
                                if 'T' in splits[0]:
                                    proteins[splits[0]] = createT(splits)
                    except IOError:
                        pass
                else:
                    continue
            except IOError:
                continue

            # Check direction for relations

            newRelations = []
            for relation in relations:
                if relation[1] in proteins:
                    newRelations.append((relation[0], relation[1], relation[2], 'NORMAL'))
                else:
                    print("Reversed")
                    newRelations.append((relation[0], relation[1], relation[2], 'REVERSED'))
            relations = newRelations

            for key in proteins:
                entities[key] = proteins[key]

            # add sentence number to all entities
            addSentenceIdToEntities(entities, None, sentences)

            # try out all possible combinations #
            for i, entity1 in enumerate(entities):
                for j, entity2 in enumerate(entities):
                    try:
                        e1_data = entities[entity1]
                        e2_data = entities[entity2]

                        # entities must be in the same sentence
                        if e1_data[3] != e2_data[3]:
                            continue

                        if j <= i:
                            continue

                        startPos1 = e1_data[1]
                        endPos1   = e1_data[2]
                        startPos2 = e2_data[1]
                        endPos2   = e2_data[2]

                        if startPos1 > endPos2:
                            startPos1 = e2_data[1]
                            endPos1   = e2_data[2]
                            startPos2 = e1_data[1]
                            endPos2   = e1_data[2]

                        e1     = text[startPos1:endPos1].strip()
                        middle = text[endPos1:startPos2].strip()
                        e2     = text[startPos2:endPos2].strip()
                        newText = "<e> " + e1 + " <e> " + middle + " <e> " + e2 + " <e>"
                        newText = ' '.join(newText.split())

                        # does the given relation exist? #
                        foundRel = None
                        for rel in relations:
                            if (entity1 == rel[1] and entity2 == rel[2]) or (entity2 == rel[1] and entity1 == rel[2]):
                                foundRel = rel
                                break
                        if foundRel:
                            newText = foundRel[0] + ";;" + newText
                            fOut.write(cleanLine(newText) + "\n")
                        else:
                            newText = "NONE;;" + newText
                            fOut.write(cleanLine(newText) + "\n")
                    except:
                        pass
            fOut.write('\n')