# -*- coding: utf-8 -*-
# Get sentences with the format <e>The dog</e> is sitting on <e>the street</e>.
# The system has to preprocess these sentences like in subtask 2 for usecases

import re
from preprocessing.semEval import cleaning2
from preprocessing import tagging, positions

def clean(sentence):
    """Clean and get all pairs of entities with context

    Args:
        param1 (str): raw input sentence with tags

    Returns:
        list: All possible relations
    
    """
    start_tag = '<e>'
    end_tag   = '</e>'

    all_entity_pos = []
    entity_samples = []
    pos_start = sentence.find(start_tag)

    # Get all sub strings #
    while pos_start != -1:
        pos_end   = sentence.find(end_tag, pos_start) + len(end_tag)
        tuple = (pos_start, pos_end)
        all_entity_pos.append(tuple)
        pos_start = sentence.find(start_tag, pos_end)

    for i, e1 in enumerate(all_entity_pos):
        for j, e2 in enumerate(all_entity_pos):
            if(j <= i):
                continue
            entity_samples.append(sentence[e1[0]:e2[1]])

    for i, sample in enumerate(entity_samples):
        if sample.count(start_tag) > 2:
            pos1 = sample.find(end_tag) + len(end_tag)
            pos2 = sample.rfind(start_tag)
            middle = sample[pos1:pos2]
            middle = re.sub(start_tag + '|' + end_tag, '', middle)
            result = sample[:pos1] + middle + sample[pos2:]
            entity_samples[i] = result
        entity_samples[i] = re.sub(end_tag, start_tag, entity_samples[i])
    
    return entity_samples

def tag(sentences):
    """ Get POS tags

    Args:
        param1 (list): all possible relations found

    Returns:
        list: Relations with POS-Tags

    """
    Returns:
    results = []
    for sentence in sentences:
        newLine = tagging.create_tags(sentence)
        results.append(newLine)

    return results

def position(sentences):
    """ Get relative positions

    Args:
        param1 (list): relations in a sentence

    Returns:
        list: relations with relative positions added

    """
    results = []
    for sentence in sentences:
        _ , newLine = positions.add_positions_to_line(sentence)
        results.append(newLine)

    return results

def preprocessMySentence(sentence):
    """Preprocess a given sentence

    Args:
        param1 (str): raw user input sentence with tags

    Returns:
        list: Preprocessed Entitiy combinations with needed features

    """
    results = clean(sentence)
    results = tag(results)
    results = position(results)
    return results