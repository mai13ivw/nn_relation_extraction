# -*- coding: utf-8 -*-
META_SPLIT = ';;'

def add_positions_to_line(line):
    """Add relative positions to each word in a relation

    Args:
        param1 (str): input line

    Returns:
        str, list: 'Relation;;direction' and positions for all words

    """ 
    text = line.split(META_SPLIT)[-1]
    meta = line.split(META_SPLIT)[:-1]

    pos = 0
    tag_found = 0
    text1 = []
    for partial in text.split('|'):
        if(tag_found >= 2):
            pos += 1
        if('<e>' in partial):
            tag_found += 1
        text1.append(pos)

    pos = 0
    tag_found = 0
    text2 = []
    for partial in reversed(text.split('|')):
        if(tag_found >= 2):
            pos -= 1
        if('<e>' in partial):
            tag_found += 1
        text2.append(pos)

    text2 = list(reversed(text2))

    line = ''
    for idx, partial in enumerate(text.split('|')):
        #get all information together
        partial = partial.replace('\n', '')
        parts = partial.split(',')
        parts.append(str(text1[idx]))
        parts.append(str(text2[idx]))
        line += ','.join(parts) + '|'
    line = line.strip('|')
    return meta, line

def add(read, write):
    """Add relative positions to all lines

    Args:
        param1 (str): input file path
        param2 (str): destination file path

    """
    with open(read, 'r', encoding='utf-8') as fIn:
        with open(write, 'w', encoding='utf-8') as fOut:
            for line in fIn:
                if line.strip() == '':
                    fOut.write('\n')
                    continue
                meta, line = add_positions_to_line(line)

                for w in line.split('|'):
                    assert(len(w.split(',')) == 4)
                    
                fOut.write(META_SPLIT.join(meta) + META_SPLIT + line + '\n')              