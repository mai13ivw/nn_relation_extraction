# -*- coding: utf-8 -*-
from tqdm import tqdm
import spacy

META_SPLIT = ';;'
lang = 'en'


def hasNumbers(input):
    """Check if an input String has digits in it

    Args:
        param1 (str): input

    Returns:
        boolean: Are there any digits?

    """
    return any(char.isdigit() for char in input)


def create_tags(line: str, lang=lang) -> str:
    """Create POS-Tags for all words using spaCy.

    The meta information part of the input line (<meta>) will not be changed.
    If line is a sentence without meta information, ";;" will still be prepended
    to the resulting sequence.

    Parameters:
    -----------
    line: str
        The line to be tagged. Format: "[<meta>;;]<sentence>"
    lang: str
        The spacy language model identifier. Defaults to 'en'.

    Returns:
    --------
    str
        The serialized POS tag sequence. Format: "[<meta>];;<sequence>"
    """

    # split meta information and sentence, remove dots
    line_split = line.split(META_SPLIT)
    meta, text = line_split[:-1], line_split[-1].replace('.', '')

    # load spacy POS tagger and analyze sentence
    nlp = spacy.load(lang, disable=['ner', 'parser'])
    doc = nlp(text)

    # process POS tag sequence
    sequence = [(t.text, t.tag_) for t in doc]
    sequence = map(lambda x: clean(*x), sequence)
    sequence = filter(None, sequence)
    sequence = map(lambda x: "{},{}".format(*x), sequence)
    l = "|".join(sequence)

    # assert result format
    for p in l.split('|'):
        assert(len(p.split(',')) == 2)

    return META_SPLIT.join(meta) + META_SPLIT + l


def clean(token: str, tag: str):
    """Clean a token tag pair.

    Replace numbers, that are not part of a proper noun by <NUM>.
    POS-Tags for <e> are defined as EEE.

    Parameters:
    -----------
    token: str
        The token.
    tag: str
        The corresponding POS tag.

    Returns:
    --------
    tuple|None
        The eventually modified tuple (token, tag) or None, if tag == ',' or token == '|'.
    """

    if(hasNumbers(token) and ('NNP' not in tag)):
        return '<NUM>', 'NUM'
    elif('<e>' in token):
        return '<e>', 'EEE'
    elif(token.strip() == '' and tag.strip() == ''):
        return ' ', 'NULL'
    elif(token.strip() == '' and tag.strip()):
        return ' ,', tag
    elif tag == ',' or token == '|':
        return None
    else:
        return token, tag


def createPOS(read, write):
    """Tag words line by line.

    Args:
        param1 (str): input file path
        param2 (str): destination file path

    """
    with open(read, 'r', encoding='utf-8') as fIn:
        with open(write, 'w', encoding='utf-8') as fOut:
            for line in tqdm(fIn):
                if line.strip() == '':
                    fOut.write('\n')
                    continue
                line = create_tags(line)
                fOut.write(line + "\n")
