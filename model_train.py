import os, sys, json, datetime, numpy as np
from keras import callbacks
from core  import keras_models, io, score, labels as myLabels
from core.embeddings import embeddings
from keras.models import model_from_json
from predicting import task1, task2
from postprocessing import task2 as post2
from weights import get_cross_entropy_weights
from tqdm import tqdm
from sklearn.utils import resample
import random

with open('model_params.json') as f:
    model_params = json.load(f)

RESOURCES_FOLDER  = 'resources/'
META_SPLIT        = ';;'

def splitCorpora(data, folds):
    """Split corpus according to the number of folds
    
    Args:
        param1 (list): contains all relations, [rel1, rel2, ..., relN]
        param2 (int): number of folds needed
    
    Returns:
        list: evenly distributed relations over all folds [[rel1, rel2], [rel3, rel4], ...]
        
    """
    foldLength = (int)(len(data) / folds)
    dataFolded = []
    for i in range(folds):
        bottom = i * foldLength
        top = (i + 1) * foldLength
        dataFolded.append(data[bottom:top])
    return dataFolded

def partialUpsamplingData(train_matrix, labels_to_upsample, reference_label, ratio, label2idx):
    """ Like normal upsampling but only for a few labels   

    You can choose what labels to upsample (they are individually upsampled!) and a reference label
    whose amount is used.

    Args:
        param1 (list): training data
        param2 (list): labels that should be upsampled
        param3 (str): a reference label that determins the upsample size
        param4 (float): ratio of param3 that will be used
        param5 (dict): mapping from labels to indices

    Returns:
        list: Like the training matrix with additional relations

    """
    # Change label names to corresponding IDs
    reference_label = label2idx[reference_label]
    for i, label in enumerate(labels_to_upsample):
        labels_to_upsample[i] = label2idx[label]

    train_majority = [sample for sample in train_matrix if sample[-1] == reference_label] # Label as upsampling reference #
    print("Amount of reference class: {}".format(len(train_majority)))

    train_minorities = []
    for label in labels_to_upsample:
        train_minority = [sample for sample in train_matrix if sample[-1] == label]
        train_minorities.append(train_minority)
        print("Amount of {} class: {}".format(label, len(train_minority)))

    other_classes = [sample for sample in train_matrix if sample[-1] not in labels_to_upsample]

    n_samples = (int)(ratio*len(train_majority))
    train_minorities_upsampled = []
    for train_minority in train_minorities:
        train_minority_upsampled = resample(train_minority,
                                        replace = True,
                                        n_samples=n_samples,
                                        random_state=321)
        train_minorities_upsampled.append(train_minority_upsampled)

    flat_train_minorities_upsampled = [item for sublist in train_minorities_upsampled for item in sublist]

    result = flat_train_minorities_upsampled + other_classes
    random.shuffle(result)
    print("#Samples after upsampling: {}".format(len(result)))
    return result

def upsampleData(train_matrix, ratio, label2idx):
    """Upsample positive examples so they match the number of examples in the negative class

    This function is helpful, when the number of neg. examples overweights pos. examples by far.

    Args:
        param1 (list): training data
        param2 (int): Set a ratio (negative to positive) with ratio in (0, 1]
        param3 (dict): mapping from labels to indices
    
    Returns:
        list: upsampled data

    >>> Upsample(myTrainMatrix, 1.0, myDict)
    #NONE = 20.000; 
    #A + #B = 500
    n = (0.5 * 20.000) / 500 = 20
    So A + B must be enlargened by 20 times
    
    """
    negativeClass = label2idx.get('NONE')
    train_majority = [sample for sample in train_matrix if sample[-1] == negativeClass] # None #
    train_minority = [sample for sample in train_matrix if sample[-1] != negativeClass] # Every other class #
    print("Amount of None class: {}".format(len(train_majority)))
    print("Amount of other classes: {}".format(len(train_minority)))

    n_samples = (int)(ratio*len(train_majority))
    train_minority_upsampled = resample(train_minority,
                                        replace = True,
                                        n_samples=n_samples,
                                        random_state=123)

    result = train_minority_upsampled + train_majority
    random.shuffle(result)
    print("#Samples after upsampling: {}".format(len(result)))
    return result

def resetShape(my_matrix):
    """Switch shape of the training matrix
    
    >>> print(resetShape([[sent1, sent2, sent3, ...], [tags1, tags2, tags3, ...], [lPos1, lPos2, lPos3, ...] ...])) 
    [[´sent1, tags1, lPos1, ...], [sent2, tags2, lPos2, ...] ...]
    
    Args:
        param1 (list): input matrix

    Returns:
        list: matrix with new shape
    
    """
    sentence_matrix  = []
    pos_matrix       = []
    lposition_matrix = []
    rposition_matrix = []
    y_matrix         = []
    for sample in my_matrix:
        sentence_matrix.append(sample[0])
        pos_matrix.append(sample[1])
        lposition_matrix.append(sample[2])
        rposition_matrix.append(sample[3])
        y_matrix.append(sample[4])
    my_matrix = [ np.asarray(sentence_matrix), 
                    np.asarray(pos_matrix), 
                    np.asarray(lposition_matrix), 
                    np.asarray(rposition_matrix), 
                    np.asarray(y_matrix)]
    return my_matrix
            
if __name__ == "__main__":
    import argparse

    ####################################################################################################
    # Load Args Parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('--domain', default='semEval', choices=['semEval', 'semEval2010', 'BioNLP', 'scienceIE'], help='Domain to choose for training.')
    parser.add_argument('--task', default='1', choices=['1', '2'], help='This parameter is only relevant for SemEval2018.')
    parser.add_argument('--model', default='LSTM', choices=['LSTM', 'CNN'], help='Model type, CNN or Bi-LSTM?')
    parser.add_argument('--epochs', '-e', default=10, help='Number of epochs to train on.')
    parser.add_argument('--amount', '-a', default=5, help='Since training is done with cross validation, amount denotes the number of splits.')

    args    = parser.parse_args()
    domain  = args.domain
    task    = args.task if domain == 'semEval' else '2'
    model   = args.model 
    epochs  = int(args.epochs)

    now  = datetime.datetime.now()
    time = now.strftime("%d%m_%H%M%S")
    name = model + '_' + time

    if(model_params[domain]['embedding'] == 50):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.50d.txt'
    elif(model_params[domain]['embedding'] == 100):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.100d.txt'
    elif(model_params[domain]['embedding'] == 200):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.200d.txt'
    elif(model_params[domain]['embedding'] == 300):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.300d.txt'
    elif(model_params[domain]['embedding'] == 0):
        WORD_EMB_PATH = RESOURCES_FOLDER + domain + '/embeddings/word_emb.txt'
    else:
        WORD_EMB_PATH = RESOURCES_FOLDER + 'easyGlove.txt'

    keras_models.initRelPositionDict(domain)

    ####################################################################################################
    # Save paths and labels
    if domain == 'semEval':
        train_path = RESOURCES_FOLDER + domain + '/preprocessed/corpora/' + task + '.training.txt'
        label2idx  = getattr(myLabels, "label2idx_semEval_" + task)
        idx2label  = {v: k for k, v in label2idx.items()}
        dest       = RESOURCES_FOLDER + domain + '/models/task_' + task + '/'
    else:
        train_path = RESOURCES_FOLDER + domain + '/preprocessed/corpora/training.txt'
        label2idx  = getattr(myLabels, "label2idx_" + domain)
        idx2label  = {v: k for k, v in label2idx.items()}
        dest       = RESOURCES_FOLDER + domain + '/models/'

    
    if not os.path.exists(dest):
            os.makedirs(dest)

    print("Word Embedding path: {}".format(WORD_EMB_PATH))
    print("Training Path: {}".format(train_path))
    print("Labels ({}): {}".format(len(label2idx), label2idx))

    ####################################################################################################
    # Load Word Embeddings and data
    word_emb_matrix, word2idx = embeddings.load(WORD_EMB_PATH)
    print("> Loaded word embedding: {}".format(word_emb_matrix.shape))
    idx2word = {v: k for k, v in word2idx.items()}

    # Training data and weights #
    train_data = io.load_data(train_path, keepBreaks=False)

    weights = None
    if(model_params[domain]['weights'] == 1):
        weights = get_cross_entropy_weights(train_data, label2idx)
        print("Weights: {}:".format(weights))

    # Load indices #
    train_matrix = keras_models.to_indices(train_data, word2idx, label2idx, domain)
    print("Train Matrix shapes: {}".format([d.shape for d in train_matrix]))

    # reshape matrices #
    new_train_matrix = []
    for newSample in zip(*train_matrix):
        new_train_matrix.append(newSample)
    train_matrix = np.asarray(new_train_matrix)

    # Get indices for upsampling #
    if(model_params[domain]['upsampling'] == 1 and task == '2'):
        train_matrix = upsampleData(train_matrix, model_params[domain]['upsample-rate'], label2idx)
    elif(model_params[domain]['upsampling'] == 2):
        if domain == 'semEval2010':
            train_matrix = partialUpsamplingData(train_matrix, ['INSTRUMENT-AGENCY_NORMAL', 'MEMBER-COLLECTION_NORMAL', 'ENTITY-DESTINATION_REVERSED'], 'MESSAGE-TOPIC_REVERSED', 1.0, label2idx)
    

    ####################################################################################################
    # Train Crossvalidation Sets
    n_folds = int(args.amount)
    train_Splits = splitCorpora(train_matrix, n_folds)
    n_out = len(label2idx)

    count = 0
    if int(args.amount) > 1:
        for i in range(n_folds):
            count += 1
            val_block   = []
            train_block = []

            for j, splitData in enumerate(train_Splits):
                if i == j:
                    val_block = splitData
                else:
                    train_block.extend(splitData)

            # Training and Validation data is loaded #
            save_json = dest + name + '_' + str(i) + '.json'
            save_h5   = dest + name + '_' + str(i) + '.h5'

            # Change format for training and validation#
            train_block  = resetShape(train_block)
            X_train_vals = np.array(train_block[:-1])
            y_train_vals = embeddings.to_one_hot(train_block[-1], n_out)
            print("Train matrix shapes: {}".format([d.shape for d in train_block]))

            val_block  = resetShape(val_block)
            X_val_vals = np.array(val_block[:-1])
            y_val_vals = embeddings.to_one_hot(val_block[-1], n_out)
            print("Validation matrix shapes: {}".format([d.shape for d in val_block]))

            

            # Build a new model by using the keras_models class #
            myModel = getattr(keras_models, model + '_model')(word_emb_matrix, 
                                                            weights,
                                                            label2idx,
                                                            task,
                                                            domain)

            myModel.summary()

            # Fit the model with the given set up and training values #
            callback_history = myModel.fit(list(X_train_vals), 
                                        y_train_vals, 
                                        epochs=epochs, 
                                        shuffle=True,
                                        batch_size=model_params['batch'], 
                                        validation_data = (list(X_val_vals), y_val_vals),
                                        verbose=1
                                        )
            scores = myModel.evaluate(list(X_val_vals), y_val_vals, verbose=0)
            print("{0}: {1}".format(myModel.metrics_names[1], scores[1]*100))
            print('> Save model...')

            # Serialize to JSON #
            model_json = myModel.to_json()
            with open(save_json, 'w') as json_file:
                json_file.write(model_json)

            # Serialize to HDF5 #
            myModel.save_weights(save_h5)
            print('> Model saved to: {}'.format(dest))

            if count >= int(args.amount):
                print('> All models trained')
                break
    else:
        print('Train a single model...')
        # Training and Validation data is loaded #
        save_json = dest + name + '.json'
        save_h5   = dest + name + '.h5'

        # Change format for training and validation#
        train_block  = resetShape(train_matrix)
        X_train_vals = np.array(train_block[:-1])
        y_train_vals = embeddings.to_one_hot(train_block[-1], n_out)

        # Build a new model by using the keras_models class #
        myModel = getattr(keras_models, model + '_model')(word_emb_matrix, 
                                                        weights,
                                                        label2idx,
                                                        task,
                                                        domain)

        # Fit the model with the given set up and training values #
        callback_history = myModel.fit(list(X_train_vals), 
                                    y_train_vals, 
                                    epochs=epochs, 
                                    shuffle=True,
                                    batch_size=64, 
                                    verbose=1
                                    )

        print('> Save model...')
        # Serialize to JSON #
        model_json = myModel.to_json()
        with open(save_json, 'w') as json_file:
            json_file.write(model_json)

        # Serialize to HDF5 #
        myModel.save_weights(save_h5)
        print('> Model saved to: {}'.format(dest))

    ####################################################################################################
    # Log files for all trained models
    log_file = dest + name + '.log'
    with open(log_file, 'w') as fOut:
        fOut.write('Domain\t{}\n'.format(domain))
        domain == 'semEval' and fOut.write('Task\t{}\n'.format(task))
        fOut.write('Glove\t{}\n'.format(model_params[domain]['embedding']))
        fOut.write('Upsampling\t{}\n'.format(model_params[domain]['upsampling']))
        fOut.write('Upsampling Ratio\t{}\n'.format(model_params[domain]['upsample-rate']))
        fOut.write('Weights\t{}\n'.format(model_params[domain]['weights']))
        fOut.write('Max Sentence\t{}\n'.format(model_params[domain]['max_sent_len']))
        fOut.write('Min Sentence\t{}\n'.format(model_params[domain]['min_sent_len']))
        fOut.write('Epochs\t{}\n'.format(epochs))
        fOut.write('Splits für Cross Validation\t{}\n'.format(args.amount))