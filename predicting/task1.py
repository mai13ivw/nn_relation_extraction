import sys, os, numpy as np
from weights import get_cross_entropy_weights, w_rnn
from core  import keras_models

module_location  = os.path.abspath(__file__)
module_location  = os.path.dirname(module_location)
RESOURCES_FOLDER = os.path.join(module_location, '../resources/')

def predictSoftmaxProbabilities(types, predict_data, model_LSTM, model_CNN, max_sent_len, min_sent_len):
    """Predict softmax probabilities for two given models

    You can also use only one model (CNN XOR LSTM)

    Args:
        param1 (list): needed for task 1 semEval-2018. Check if the relation is reversed. If so, it can't be COMPARE
        param2 (list): preprocessed inputs that need a prediction
        param3 (Keras.model): LSTM model
        param4 (Keras.model): CNN model
        param5 (int): size of longest sentence in training data
        param6 (int): size of shortest sentence in training data

    Returns:
        list: predictions as probability list for each input relation

    """
    if not model_LSTM or not model_CNN:
        return None

    sentences    = predict_data[0]
    max_sent_len = max_sent_len / 2 # size in json is set longer (just in case). So reduce it's influence here

    pred_weights = []
    for sent in sentences:
        w = w_rnn(sent, min_sent_len, max_sent_len)
        pred_weights.append(w)

    predictions_LSTM = model_LSTM.predict(predict_data)
    predictions_CNN  = model_CNN.predict(predict_data)

    predictions = []
    for l, c, w, type in zip(predictions_LSTM, predictions_CNN, pred_weights, types):
        if 'REVERSED' in type:
            l[5] = -1
            c[5] = -1
        
        pred = l * w + c * (1-w)
        predictions.append(pred)
    return predictions

def predict(types, predict_data, model_LSTM, model_CNN, max_sent_len, min_sent_len, idx2label):
    """Turn prediction probabilities to label predictions

    The input models can be CNN, LSTM or CNN + LSTM

    Args:
        param1 (list): needed for task 1 semEval-2018. Check if the relation is reversed. If so, it can't be COMPARE
        param2 (list): preprocessed inputs that need a prediction
        param3 (Keras.model): LSTM model
        param4 (Keras.model): CNN model
        param5 (int): size of longest sentence in training data
        param6 (int): size of shortest sentence in training data
        param7 (dict): mapping from indices to labels

    Returns:
        list: predictions as labels for each input relation

    """
    labeledPredictions = []
    softmaxPredictions = predictSoftmaxProbabilities(types, predict_data, model_LSTM, model_CNN, max_sent_len, min_sent_len)
    for p in softmaxPredictions:
        labeledPredictions.append(idx2label[np.argmax(p)])
    return labeledPredictions

def predictMany(types, predict_data, combined_models, max_sent_len, min_sent_len, idx2label):
    """Predict with multiple pairs of LSTMs and CNNs (ensamble)

    Args:
        param1 (list): needed for task 1 semEval-2018. Check if the relation is reversed. If so, it can't be COMPARE
        param2 (list): preprocessed inputs that need a prediction
        param3 (list): contains tuples of CNNs and LSTMs [(CNN1, LSTM1), (CNN2, LSTM2), ...]
        param5 (int): size of longest sentence in training data
        param6 (int): size of shortest sentence in training data
        param7 (dict): mapping from indices to labels

    Returns:
        list: predictions as labels for each input relation   

    """
    # Get multiple arrays with predictions as arrays #
    predictions = []
    for models in combined_models:
        prediction = predictSoftmaxProbabilities(types, predict_data, models[1], models[2], max_sent_len, min_sent_len)
        predictions.append(prediction)
    
    zipped = zip(*predictions)
    results = []
    for preds in zipped:
        avg = np.zeros(preds[0].shape)
        for p in preds:
            avg += p
        avg /= len(preds)
        results.append(avg)
    
    labeledPredictions = []
    for r in results:
        labeledPredictions.append(idx2label[np.argmax(r)])
    
    return labeledPredictions

def savePredictions(predictions):
    """ Save predictions for task 1 

    This is done for all keys.test.X for tasks 1
    The results are saved to keys.pred.X

    Args:
        param1 (list): all predictions

    """
    predictions = [pred for p in predictions for pred in p]

    if not os.path.exists(RESOURCES_FOLDER + 'semEval/predicted/'):
            os.makedirs(RESOURCES_FOLDER + 'semEval/predicted/')

    keyFiles = [[RESOURCES_FOLDER + 'semEval/raw/keys.test.1.1.txt', RESOURCES_FOLDER + 'semEval/predicted/keys.pred.1.1.txt'], 
                [RESOURCES_FOLDER + 'semEval/raw/keys.test.1.2.txt', RESOURCES_FOLDER + 'semEval/predicted/keys.pred.1.2.txt']]

    predPointer = 0
    for data in keyFiles:
        key     = data[0]
        dest    = data[1]
        with open(dest, 'w') as fOut:
            with open(key, 'r') as fIn:
                for line in fIn:
                    line = line.replace('\n', '')
                    newLine = line.split('(')[-1]
                    newLine = predictions[predPointer] + '(' + newLine

                    predPointer += 1
                    fOut.write(newLine + '\n')