import sys, numpy as np
from weights import get_cross_entropy_weights, w_rnn
from core  import keras_models

def predictSoftmaxProbabilities(predict_data, model_LSTM, model_CNN, max_sent_len, min_sent_len):
    """Predict softmax probabilities for two given models

    You can also use only one model (CNN XOR LSTM)

    Args:
        param2 (list): preprocessed inputs that need a prediction
        param3 (Keras.model): LSTM model
        param4 (Keras.model): CNN model
        param5 (int): size of longest sentence in training data
        param6 (int): size of shortest sentence in training data

    Returns:
        list: predictions as probability list for each input relation

    """
    
    # Without models no predictions are possible #
    if not model_LSTM and not model_CNN:
        return []

    sentences    = predict_data[0]
    max_sent_len = max_sent_len / 2

    # get weights for cnn and rnn per sentence, depending on the sentence's length #
    pred_weights = []
    for sent in sentences:
        w = w_rnn(sent, min_sent_len, max_sent_len)
        pred_weights.append(w)

    # Check if model exists, predictions without one model are possible as well #
    if model_LSTM:
        predictions_LSTM = model_LSTM.predict(predict_data)

    if model_CNN:
        predictions_CNN  = model_CNN.predict(predict_data)

    # postprocess all predictions with weights #
    predictions = []

    if model_LSTM and model_CNN:
        for l, c, w in zip(predictions_LSTM, predictions_CNN, pred_weights):
            pred = l * w + c * (1-w)
            predictions.append(pred)
    elif model_LSTM and not model_CNN:
        predictions = predictions_LSTM
    elif not model_LSTM and model_CNN:
        predictions = predictions_CNN

    return predictions
    
def predict(predict_data, model_LSTM, model_CNN, max_sent_len, min_sent_len, idx2label):
    """Turn prediction probabilities to label predictions

    The input models can be CNN, LSTM or CNN + LSTM

    Args:
        param2 (list): preprocessed inputs that need a prediction
        param3 (Keras.model): LSTM model
        param4 (Keras.model): CNN model
        param5 (int): size of longest sentence in training data
        param6 (int): size of shortest sentence in training data
        param7 (dict): mapping from indices to labels

    Returns:
        list: predictions as labels for each input relation

    """
    predictions        = predictSoftmaxProbabilities(predict_data, model_LSTM, model_CNN, max_sent_len, min_sent_len)
    labeledPredictions = [idx2label[np.argmax(v)] for v in predictions]
    softmaxValues      = [np.amax(m) for m in predictions]
    return labeledPredictions, softmaxValues

def avgResults(zippedPredictions):
    """Use average function for an ensamle
    
    Args:
        param1 (list): all predictions as softmax probabilities for multiple models

    Returns:
        list: average probabilities for each relation

    """
    results = []
    for preds in zippedPredictions:
        avg = np.zeros(preds[0].shape)
        for p in preds:
            avg += p
        avg /= len(preds)
        results.append(avg)
    return results

def majorityResults(zippedPredictions):
    """Use majority function for an ensamle
    
    Args:
        param1 (list): all predictions as softmax probabilities for multiple models

    Returns:
        list: average probabilities for each relation

    """
    results = []
    for preds in zippedPredictions:
        result = np.zeros(preds[0].shape)
        for p in preds:
            vote = np.argmax(p)
            result[vote] += 1
        idx = np.argmax(result)
        result = np.zeros(preds[0].shape)
        result[idx] = 1
        results.append(result)
    return results

def predictMany(predict_data, combined_models, max_sent_len, min_sent_len, idx2label):
    """Predict with multiple pairs of LSTMs and CNNs (ensamble)

    Args:
        param2 (list): preprocessed inputs that need a prediction
        param3 (list): contains tuples of CNNs and LSTMs [(CNN1, LSTM1), (CNN2, LSTM2), ...]
        param5 (int): size of longest sentence in training data
        param6 (int): size of shortest sentence in training data
        param7 (dict): mapping from indices to labels

    Returns:
        list: predictions as labels for each input relation   

    """
    predictions = []
    for models in combined_models:
        prediction = predictSoftmaxProbabilities(predict_data, models[1], models[2], max_sent_len, min_sent_len)
        predictions.append(prediction)
    
    zipped = zip(*predictions)

    ## avgResults(zipped)
    ## majorityResults(zipped)
    results = majorityResults(zipped)

    labeledPredictions = [idx2label[np.argmax(r)] for r in results]
    softmaxValues      = [np.amax(r) for r in results]
    
    return labeledPredictions, softmaxValues