# Check different POS-Tag distributions and positions
# The top 6 POS-Tags fpr each relation are printed
# Example:

# USAGE_NORMAL
#         NN: 32.34% ; Tag kam 1850 Mal vor.
#         > cE1: 3.57%, E1: 42.0%, c: 4.05%, cE2: 10.92%, E2: 39.46%

# E1: Tag was part of entity 1
# cE1: Tag is part of the middle context (left 33%)
# c: Tag is part of the middle context (middle 33%)
# cE2: Tag is part of the middle context (right 33%)
# E2: Tag was part of entity 2

import operator

RESOURCES_FOLDER  = 'resources/'
META_SPLIT = ';;'

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--domain', default='semEval', choices=['semEval', 'semEval2010', 'BioNLP', 'scienceIE'])
    
    args = parser.parse_args()
    domain = args.domain

    # get path to corpus training file
    if domain == 'semEval':
        path = RESOURCES_FOLDER + domain + '/preprocessed/corpora/2.training.txt'
    else:
        path = RESOURCES_FOLDER + domain + '/preprocessed/corpora/training.txt'

    relations = {}
    pos_tag_positions = {}
    with open(path, 'r') as fIn:
        for line in fIn:
            if line.strip() == '':
                continue
            relation = '_'.join(line.split(META_SPLIT)[:-1])
            text = line.split(META_SPLIT)[-1]

            if relation != 'NONE':
                if relation in relations:
                    pos_counts = relations[relation]  
                else:
                    pos_counts = {}

                if relation not in pos_tag_positions:
                    pos_tag_positions[relation] = {}

                for i, block in enumerate(text.split('|')):
                    # get relative positions for pos tags
                    rel1 = block.split(',')[2]
                    rel2 = block.split(',')[3]

                    if rel1 == '1':
                        start = i
                    if rel2 == '-1':
                        end = i
                middleLength = end - start + 1

                for i, block in enumerate(text.split('|')):
                    if middleLength == 1 and i == start:
                        pos = 'c'
                    elif i < start:
                        pos = 'E1'
                    elif i > end:
                        pos = 'E2'
                    elif i < start + (middleLength / 3):
                        pos = 'cE1'
                    elif i >= start + ((middleLength * 2) / 3) - 1:
                        pos = 'cE2'
                    else:
                        pos = 'c'
                    tag = block.split(',')[1]
                    if tag in pos_tag_positions[relation]:
                        pos_tag_positions[relation][tag][pos] += 1
                    else:
                        pos_tag_positions[relation][tag] = {'E1':0, 'cE1':0, 'c':0, 'cE2':0, 'E2':0}
                        pos_tag_positions[relation][tag][pos] += 1

                    if tag in pos_counts:
                        pos_counts[tag] += 1
                    else:
                        pos_counts[tag] = 1
                relations[relation] = pos_counts

        for relation in relations:
            tags = relations[relation]
            del tags['EEE']
            N = sum([tags[v] for v in tags])

            sorted_tags = sorted(tags.items(), reverse=True, key=operator.itemgetter(1))

            print(relation)
            for i, tag in enumerate(sorted_tags):
                if i > 5: 
                    break
                print('\t{}: {}% ; Tag kam {} Mal vor.'.format(tag[0], round((100 * tag[1])/N, 2), tag[1]))
                result = ', '.join(['{}: {}%'.format(position, round(100 * pos_tag_positions[relation][tag[0]][position] / tag[1], 2)) for position in pos_tag_positions[relation][tag[0]]])
                print("\t> " + result)
            #print('{}: {}'.format(relation, relations[relation]))

            
