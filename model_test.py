import os, sys, json
from core import keras_models, io, score, labels as myLabels
from core.loadModel import loadModel
from core.embeddings import embeddings
from keras.models import model_from_json
from predicting import task1, task2
from postprocessing import task2 as post2
from tqdm import tqdm

from keras import backend as K
from pprint import pprint

with open('model_params.json') as f:
    model_params = json.load(f)

RESOURCES_FOLDER  = 'resources/'
META_SPLIT        = ';;'
        
if __name__ == "__main__":
    import argparse

    ####################################################################################################
    # Load Args Parameters
    parser = argparse.ArgumentParser()
    parser.add_argument('--domain', default='semEval')
    parser.add_argument('--task', default='1')
    parser.add_argument('--cnnModel', default='')
    parser.add_argument('--lstmModel', default='')

    args    = parser.parse_args()
    task    = args.task
    domain  = args.domain
    lstm    = args.lstmModel
    cnn     = args.cnnModel

    if(model_params[domain]['embedding'] == 50):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.50d.txt'
    elif(model_params[domain]['embedding'] == 100):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.100d.txt'
    elif(model_params[domain]['embedding'] == 200):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.200d.txt'
    elif(model_params[domain]['embedding'] == 300):
        WORD_EMB_PATH = RESOURCES_FOLDER + 'glove.6B/glove.6B.300d.txt'
    elif(model_params[domain]['embedding'] == 0):
        WORD_EMB_PATH = RESOURCES_FOLDER + domain + '/embeddings/word_emb.txt'
    else:
        WORD_EMB_PATH = RESOURCES_FOLDER + 'easyGlove.txt'

    keras_models.initRelPositionDict(domain)
    
    ####################################################################################################
    # Save paths and labels
    if domain == 'semEval':
        val_path  = RESOURCES_FOLDER + domain + '/preprocessed/corpora/' + task + '.validation.txt'
        label2idx = getattr(myLabels, "label2idx_semEval_" + task)
        idx2label  = {v: k for k, v in label2idx.items()}
    else:
        val_path  = RESOURCES_FOLDER + domain + '/preprocessed/corpora/validation.txt'
        label2idx = getattr(myLabels, "label2idx_" + domain)
        idx2label  = {v: k for k, v in label2idx.items()}

    print("Word Embedding path: {}".format(WORD_EMB_PATH))
    print("Validation Path: {}".format(val_path))
    print("Labels ({}): {}".format(len(idx2label), idx2label))
    
    ####################################################################################################
    # Load Word Embeddings and models
    word_emb_matrix, word2idx = embeddings.load(WORD_EMB_PATH)
    print("> Loaded word embedding: {}".format(word_emb_matrix.shape))

    # Load relevant models #
    loaded_model_lstm = None
    loaded_model_cnn  = None
    if lstm:
        loaded_model_lstm = loadModel(lstm, domain, task, type='LSTM')
    if cnn:
        loaded_model_cnn  = loadModel(cnn, domain, task, type='CNN')

    maxSent = model_params[domain]['max_sent_len']
    minSent = model_params[domain]['min_sent_len']

    ####################################################################################################
    # Predictions for task 1 and semEval
    if domain == 'semEval' and task == '1':
        types = []
        val_NR_path = RESOURCES_FOLDER + domain + '/preprocessed/corpora/1.validation.NR.txt'
        with open(val_NR_path, 'r') as fIn:
            for line in fIn:
                types.append(line)

        predict_data   = io.load_data(val_path, keepBreaks=True)
        predict_matrix = keras_models.to_indices(predict_data, word2idx, label2idx, domain)
        predict_data   = None

        # Check layer
        posLayer = K.function([loaded_model_cnn.layers[0].input],
                                        [loaded_model_cnn.layers[5].output])
        pprint(vars(posLayer))
        out = posLayer([predict_matrix[1]]) #POS Tags
        sys.exit(0)

        # Get all predictions #
        predictions = [task1.predict(types, predict_matrix[:-1], loaded_model_lstm, loaded_model_cnn, maxSent, minSent, idx2label)]
        scores = score.getMacroF1(label2idx, idx2label, predictions, [predict_matrix], domain)
        score.printScores(scores)
        sys.exit(0)

    ####################################################################################################
    # Any other predictions for task 2 and domains
    predict_data_set = io.load_data_2(val_path)

    # Indices are saved for every sentence seperately #
    predict_matrices = []
    for predict_data in predict_data_set:
        p_data = keras_models.to_indices(predict_data, word2idx, label2idx, domain)
        predict_matrices.append(p_data)

    print("Amount of predict matrices: {}".format(len(predict_matrices)))

    prediction_set = []
    probability_set = []
    for predict_matrix in tqdm(predict_matrices):
        predictions, probabilities = task2.predict(predict_matrix[:-1], loaded_model_lstm, loaded_model_cnn, maxSent, minSent, idx2label)
        prediction_set.append(predictions)
        probability_set.append(probabilities)

    if domain == 'semEval':
        print('Delete multiple relations...')
        prediction_set = post2.postProcessing(predict_data_set, prediction_set, probability_set)
    
    scores = score.getMacroF1(label2idx, idx2label, prediction_set, predict_matrices, domain)
    score.printScores(scores)